
CREATE OR REPLACE FUNCTION set_product_weight_code() RETURNS TRIGGER AS $BODY$
  DECLARE
    _value bigint;
    _shop_id text;
  BEGIN
    if ( NEW.is_fractionary = true ) then
      _shop_id := replace(NEW.shop_id::text, '-', '_');
      BEGIN
        _value := nextval('products_weight_code_' || _shop_id);
        NEW.weight_code := to_char(_value, 'FM00000');
      EXCEPTION WHEN OTHERS THEN
      END;
    end if;
    RETURN NEW;
  END;
$BODY$ LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS generate_product_weight_code ON products;
CREATE TRIGGER generate_product_weight_code
  BEFORE INSERT ON products
    FOR EACH ROW EXECUTE PROCEDURE set_product_weight_code();