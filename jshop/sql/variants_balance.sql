DROP FUNCTION IF EXISTS variants_balance( uuid[], uuid, uuid, timestamp without time zone, timestamp without time zone );
CREATE OR REPLACE FUNCTION variants_balance( _variants_ids uuid [], _stock_id uuid, _shop_id uuid, _from timestamp without time zone, _to timestamp without time zone )
  RETURNS TABLE( json json ) AS $BODY$
  DECLARE
begin
  if (_from > _to) then _from = _to;
  end if;
  RETURN query
    Select array_to_json(array_agg(row_to_json(t)))
    From(
      WITH rates AS (
        Select sc.currency_id   AS currency_id,
               COALESCE((
                  Select value From shop_currency_rates scr
                  Where scr.shop_currency_id = sc.id
                    And scr.apply_at <= _to
                  Order By scr.apply_at desc, scr.created_at desc
                  Limit 1
                ), 1.0)         AS rate_value
        From shop_currencies sc
        Where sc.shop_id = _shop_id
      ),
      incomes AS (
        Select ip.unit_count                              AS unit_count,
               d.document_type                            AS document_type,
               d.document_date                            AS document_date,
               ip.product_id                              AS variant_id,
               d.currency_id                              AS currency_id,
               ip.per_unit_cost_price                     AS cost_price,
               ip.unit_count - COALESCE(sum(gf.count), 0) AS balance
        From invoice_products ip
        Inner Join documents d On d.id = ip.document_id
        Left  Join goods_flows gf On gf.invoice_product_id = ip.id
                                 And gf.date <= _to
        Where ip.is_active = true
          And d.shop_id = _shop_id
          And d.stock_id = _stock_id
          And d.status = 0
          And d.document_date <= _to
        Group By ip.unit_count, d.document_type, d.document_date, ip.product_id, d.currency_id, ip.per_unit_cost_price
      ),
      outcomes AS (
        Select gf.count          AS unit_count,
               gf.date           AS document_date,
               gf.operation_type AS document_type,
               gf.variant_id     AS variant_id
        From goods_flows gf
        Where gf.shop_id = _shop_id
          And gf.stock_id = _stock_id
          And gf.date <= _to
      )
      SELECT s._variant_id as variant_id,
        COALESCE(
        ( Select sum(i.balance * (i.cost_price * r.rate_value))
          From incomes i
          Inner Join rates r On i.currency_id = r.currency_id
          Where i.variant_id = s._variant_id
        ), 0) AS cost_price_amount,
        COALESCE(
        ( Select sum(i.unit_count)
          From incomes i
          Where i.document_date < _from
            And i.variant_id = s._variant_id
        ), 0) AS beginnig_income,
        COALESCE(
        ( Select sum(i.unit_count)
          From incomes i
          Where i.document_date <= _to
            And i.document_date >= _from
            And i.document_type = 'invoice'
            And i.variant_id = s._variant_id
        ), 0) AS invoice_income,
        COALESCE(
        ( Select sum(i.unit_count)
          From incomes i
          Where i.document_date <= _to
            And i.document_date >= _from
            And i.document_type = 'relocation'
            And i.variant_id = s._variant_id
        ), 0) AS relocation_income,
        COALESCE(
        ( Select sum(i.unit_count)
          From incomes i
          Where i.document_date <= _to
            And i.document_date >= _from
            And i.document_type = 'inventory'
            And i.variant_id = s._variant_id
        ), 0) AS inventory_income,
        COALESCE(
        ( Select sum(o.unit_count)
          From outcomes o
          Where o.document_date < _from
            And o.variant_id = s._variant_id
        ), 0) AS beginnig_outcome,
        COALESCE(
        ( Select sum(o.unit_count)
          From outcomes o
          Where o.document_date <= _to
            And o.document_date >= _from
            And o.document_type = 1
            And o.variant_id = s._variant_id
        ), 0) AS write_off_outcome,
        COALESCE(
        ( Select sum(o.unit_count)
          From outcomes o
          Where o.document_date <= _to
            And o.document_date >= _from
            And o.document_type = 2
            And o.variant_id = s._variant_id
        ), 0) AS relocation_outcome,
        COALESCE(
        ( Select sum(o.unit_count)
          From outcomes o
          Where o.document_date <= _to
            And o.document_date >= _from
            And o.document_type = 4
            And o.variant_id = s._variant_id
        ), 0) AS inventory_outcome,
        COALESCE(
        ( Select sum(o.unit_count)
          From outcomes o
          Where o.document_date <= _to
            And o.document_date >= _from
            And o.document_type = 5
            And o.variant_id = s._variant_id
        ), 0) AS sales_invoice_outcome,
        COALESCE(
        ( Select sum(o.unit_count)
          From outcomes o
          Where o.document_date <= _to
            And o.document_date >= _from
            And o.document_type = 6
            And o.variant_id = s._variant_id
        ), 0) AS offline_sales_outcome,
        COALESCE(
        ( Select sum(o.unit_count)
          From outcomes o
          Where o.document_date <= _to
            And o.document_date >= _from
            And o.document_type = 3
            And o.variant_id = s._variant_id
        ), 0) AS bill_outcome
      FROM unnest(array[_variants_ids]) AS s(_variant_id)
    ) t;
  END;
$BODY$ LANGUAGE plpgsql;
