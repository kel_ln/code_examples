
DROP FUNCTION IF EXISTS convert_number_to_sku( bigint );
CREATE OR REPLACE FUNCTION convert_number_to_sku( _sequence_value bigint )
  RETURNS character varying AS $BODY$
  DECLARE
    _quotient integer;
    _number_part integer;
    _division_shift smallint := 0;
    _modulo integer;
    _symbol_part character varying;
    _result_sku character varying;
  BEGIN
    if ( _sequence_value <= 9999 ) then
      _result_sku = chr(65) || to_char(_sequence_value, 'FM0000');
    else
      _quotient := _sequence_value / 9999;
      _number_part := _sequence_value - (_quotient * 9999);
      if ( _number_part = 0 ) then
        _number_part := 9999;
        _quotient := _quotient - 1;
      end if;

      if ( _quotient <= 25 ) then
        _symbol_part := chr(65 + _quotient);
      else

        while ( _quotient > 0 ) loop
          _modulo := _quotient % 26 - _division_shift;

          if ( _modulo < 0 AND _quotient < 26 ) then
            exit;
          end if;

          _quotient := _quotient / 26;

          if ( _modulo = -1 ) then
            _modulo := 25;
            _division_shift := 2;
          elsif ( _modulo = -2 ) then
            _modulo := 24;
            _division_shift := 1;
          else
            _division_shift := 1;
          end if;

          _symbol_part := concat(_symbol_part, chr(65 + _modulo));
        end loop;

        _symbol_part := reverse(_symbol_part);

      end if;

      _result_sku := _symbol_part || to_char(_number_part, 'FM0000');
    end if;
    RETURN _result_sku;
  END
$BODY$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION set_product_sku() RETURNS TRIGGER AS $BODY$
  DECLARE
    _sequence_value bigint;
  BEGIN
    _sequence_value := nextval('products_sku');

    NEW.sku := convert_number_to_sku(_sequence_value);
    RETURN NEW;
  END;
$BODY$ LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS generate_product_sku ON products;
CREATE TRIGGER generate_product_sku
  BEFORE INSERT ON products
    FOR EACH ROW EXECUTE PROCEDURE set_product_sku();
