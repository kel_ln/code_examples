# frozen_string_literal: true

class V1::ExcelExport::Products < ::V1::ExcelExport::ExportService
  @@columns = ["number", "name", "sku", "unit", "category", "tags", "tax", "barcodes", "weight_code"].freeze

  @@transalate_path = "export.products".freeze

  def call
    columns = @additional_params.blank? ? @@columns : @additional_params.split(",")
    @excluded_columns = @@columns - columns
    excluded_indexes = @excluded_columns.map { |t| @@columns.index(t) }.compact

    set_header columns, @@transalate_path

    @attributes.each_with_index do |product, index|

      tags = product.tags.select { |c| c.is_active }.pluck(:name).join("; ")
      barcodes = product.active_product_barcodes.select { |c| c.is_active }.map { |b| "#{b.barcode}" }.join("; ")

      row = [ index + 1, product.name, product.sku, product.unit.symbol, product.category.name, tags,
        product.shop_tax.value, barcodes, product.weight_code ]
      styles = [ @center_f, @left_f, @center_f, @center_f, @left_f, @text_f, @center_f, @text_f, @center_f ]
      types = [:integer, :string, :string, :string, :string, :string, :integer, :string, :string]

      excluded_indexes.reverse.each { |del| row.delete_at(del); styles.delete_at(del); types.delete_at(del) } if !excluded_indexes.blank?

      @ws.add_row row, style: styles, types: types
    end

    convert_to_string
  end
end
