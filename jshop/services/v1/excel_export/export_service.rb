# frozen_string_literal: true

class V1::ExcelExport::ExportService
  attr_reader :attributes, :excluded_columns, :additional_params

  def initialize(attributes = {}, excluded_columns = [], additional_params = nil)
    @attributes = attributes
    @excluded_columns = excluded_columns
    @additional_params = additional_params

    @page = Axlsx::Package.new
    @page.workbook.styles.fonts.first.sz = 12
    @ws = @page.workbook.add_worksheet

    @center_f                 = @ws.styles.add_style alignment: { horizontal: :center, vertical: :center }, border: Axlsx::STYLE_THIN_BORDER
    @right_f                  = @ws.styles.add_style alignment: { horizontal: :right,  vertical: :center }, border: Axlsx::STYLE_THIN_BORDER
    @left_f                   = @ws.styles.add_style alignment: { horizontal: :left,   vertical: :top,   wrap_text: true }, border: Axlsx::STYLE_THIN_BORDER
    @right_bold_percentage_f  = @ws.styles.add_style alignment: { horizontal: :right,  vertical: :center }, border: Axlsx::STYLE_THIN_BORDER, format_code: "0.00%", b: true
    @right_bold_text_f        = @ws.styles.add_style alignment: { horizontal: :right,  vertical: :center, wrap_text: true }, border: Axlsx::STYLE_THIN_BORDER, b: true


    @header_f                 = @ws.styles.add_style alignment: { horizontal: :center, vertical: :center }, border: Axlsx::STYLE_THIN_BORDER, b: true
    @date_f                   = @ws.styles.add_style alignment: { horizontal: :right,  vertical: :center }, border: Axlsx::STYLE_THIN_BORDER, format_code: "m/d/yy h:mm"
    @text_f                   = @ws.styles.add_style alignment: { horizontal: :left,   vertical: :top,   wrap_text: true }, border: Axlsx::STYLE_THIN_BORDER
    @currency_f               = @ws.styles.add_style alignment: { horizontal: :right,  vertical: :center }, border: Axlsx::STYLE_THIN_BORDER, format_code: "#,##0.00"
    @count_f                  = @ws.styles.add_style alignment: { horizontal: :center, vertical: :center }, border: Axlsx::STYLE_THIN_BORDER, format_code: "#,##0.0###"
  end

  def set_header(columns, translation_path)
    @ws.add_row Array.new(columns.size) { |c| I18n.t("#{translation_path}.#{columns[c]}") }, style: Array.new(columns.size, @header_f)
  end

  def convert_to_string
    io = StringIO.new
    @page.use_shared_strings = true
    io.write(@page.to_stream.read)
    io.string
  end

  class << self
    def call(*args)
      new(*args).call
    end
  end
end
