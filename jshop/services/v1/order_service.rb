# frozen_string_literal: true

class V1::OrderService
  attr_reader :order, :shop, :trade_point, :user, :loyalty_card, :loyalty_program

  def initialize(shop, trade_point, user)
    @shop             = shop
    @user             = user
    @trade_point      = trade_point
    @stock            = @trade_point.stock
    @loyalty_program  = nil
    @loyalty_card     = nil
    @debt_transaction = nil
    @order            = nil
  end


  def build_new_order(params)
    @order = @shop.orders.build params
    @order.user_id        = @user.id
    @order.stock_id       = @trade_point.stock_id
    @order.cashbox_id     = @trade_point.cashbox_id
    @order.trade_point_id = @trade_point.id
    @order.date           = Time.current
    @order.order_type     = @order.is_draft ? "drafted" : "saved"
  end

  def build_draft_order(order, params)
    @order = order

    if !@order.drafted?
      @order.errors.add(:order_type, I18n.t("errors.can_update_only_draft_orders"), code: 704)
    end

    # if @order.draft_update_period_expired?
    #   @order.errors.add(:date, I18n.t("errors.draft_order_lifetime_expired"), code: 703)
    # end

    @order.assign_attributes(params)
    @order.user_id    = @user.id
    @order.date       = Time.current
    @order.order_type = "saved"

    @order.order_variants.each(&:mark_for_destruction)
  end


  def build_order_for_payment(order)
    @order           = order
    @loyalty_card    = @order.loyalty_card
    @loyalty_program = @order.loyalty_program

    if !@order.saved?
      @order.errors.add(:order_type, I18n.t("errors.only_saved_bills_are_allowed_to_pay"), current_order_status: @order.order_type)
    end
  end

  def build_saved_order(order)
    @order           = order
    @loyalty_card    = @order.loyalty_card
    @loyalty_program = @order.loyalty_program

    if !@order.saved?
      @order.errors.add(:order_type, I18n.t("errors.only_saved_bills_are_allowed_to_pay"), current_order_status: @order.order_type)
    end
  end

  def build_order_for_debt_pay(order)
    @order            = order
    @loyalty_card     = @order.loyalty_card
    @loyalty_program  = @order.loyalty_program
    @debt_transaction = @order.transactions.find_by(transaction_type: "accruing_debt")

    if @debt_transaction.blank?
      @order.errors.add("order debt transaction", I18n.t("errors.order_has_no_debts"))
    elsif @debt_transaction.converted_amount == 0
      @order.errors.add("order debt transaction", I18n.t("errors.debt_was_repaid"))
    end
  end

  def prepare_order
    if !@order.shop_assistant_id.blank?
      sc_percent = @order.shop_assistant&.sales_commission(@shop.id)
      if !sc_percent.blank?
        @order.sales_commission_percent = sc_percent
        @order.sales_commission_type    = @shop.sales_commission_type
      else
        @order.errors.add(:shop_assistant_id, I18n.t("errors.shop_assistant_not_found"), code: 712)
      end
    end

    if !@order.loyalty_card_id.blank?
      @loyalty_card = @shop.loyalty_cards.active.activated.active_client.with_valid_thru.find_by(id: @order.loyalty_card_id)
      if @loyalty_card.blank?
        @order.errors.add(:loyalty_card_id, I18n.t("errors.loyalty_card_not_found"), code: 850)
      else
        @loyalty_program = @shop.loyalty_programs.active.activated.find_by(id: @loyalty_card.loyalty_card_serie.loyalty_program_id)
        if @loyalty_program.blank?
          @order.errors.add(:loyalty_card_id, I18n.t("errors.loyalty_program_not_found"), code: 800)
        else
          @order.shop_client_id          = @loyalty_card.shop_client_id
          @order.loyalty_program_id      = @loyalty_program.id
          @order.loyalty_program_percent = @loyalty_program.loyalty_percent
          @order.loyalty_program_type    = @loyalty_program.loyalty_type
          @order.loyalty_program_status  = "verification"
        end
      end
    end
  end

  def prepare_order_variants(params, allocate_batches = true)
    if !params[:order_products].blank?

      variant_ids     = params[:order_products].pluck(:variant_id)
      variants_prices = Variant.search_sell_products_batches(@shop.id, variant_ids, @stock.id, @stock.write_off_type, @user.id)
      variants        = @shop.variants.all_active.where(id: variant_ids).includes(product: :shop_tax)

      order_products  = group_order_products params[:order_products]

      order_products.each do |item|

        item = item.symbolize_keys if item.is_a?(Hash)

        variant = variants.find { |r| r[:id] == item[:variant_id] }

        if item[:count].to_f <= 0
          @order.errors.add("variant - #{item[:variant_id]}", I18n.t("errors.cant_create_item_with_zero_count"), code: 713)
        end

        if variant.blank?
          @order.errors.add("variant - #{item[:variant_id]}", I18n.t("errors.variant_not_found"), code: 714)
        elsif item[:count].to_f % 1 > 0 && !variant.product.is_fractionary
          @order.errors.add("variant - #{item[:variant_id]}", I18n.t("errors.product_not_fractional"), code: 711)
        else
          sell_price, cost_price = nil

          batch = if @stock.is_manual_write_off?
            variants_prices[variant.id]&.detect { |r| r[:batch_id] == item[:batch_object_id] && r[:price_list_id] == item[:price_list_id] }
          else
            variants_prices[variant.id]&.detect { |r| r[:sell_price].to_f == item[:price].to_f && r[:expiration_date] == item[:expiration_date] && r[:price_list_id] == item[:price_list_id] }
          end

          if !batch.blank?

            bt_balance = batch[:balance_per_packs].detect { |bpp| bpp[:unit_id] == item[:unit_id] }
            bt_balance = bt_balance.blank? ? 0 : bt_balance[:balance]
            sell_price = batch[:sell_price]
            cost_price = batch[:cost_price]

            next if bt_balance == 0 && batch[:can_delete]

            if bt_balance.to_f < item[:count].to_f
              @order.errors.add("variant - #{item[:variant_id]}", I18n.t("errors.batch_balance_less_then_count"), code: 715)
              item[:count] = bt_balance.to_f # максимальное доступное количество
            end

            if sell_price.to_f != item[:price].to_f
              @order.errors.add("variant - #{item[:variant_id]}", "price changed current price = #{sell_price}, received = #{item[:price]}", code: 716)
            else # if item[:count].to_f > 0
              c = item[:count].to_f

              loop do
                order_product = @order.order_variants.build item

                order_product.shop_id        = @shop.id
                order_product.shop_tax       = variant.product.shop_tax
                order_product.is_fractionary = variant.product.is_fractionary
                order_product.category_id    = variant.product.category_id

                order_product.convert_to_units

                if !@stock.is_manual_write_off?
                  if allocate_batches || c == 0 || order_product.is_fractionary
                    grouped_batch = if c == 0
                      batch[:auto_groupped][0]
                    elsif order_product.is_product_pack? || !order_product.is_fractionary
                      batch[:auto_groupped].select { |k| k[1] > 0 && k[1] >= order_product.units_per_pack }.min_by(&:second)
                    else
                      batch[:auto_groupped].select { |k| k[1] > 0 }.min_by(&:second)
                    end

                    order_product.count = if grouped_batch[1] < c * order_product.units_per_pack
                      if (order_product.is_product_pack? || !order_product.is_fractionary)
                        (grouped_batch[1] / order_product.units_per_pack).to_i
                      else
                        grouped_batch[1]
                      end
                    else
                      c
                    end

                    order_product.convert_to_units
                    order_product.batch_object_id = grouped_batch[0]
                    grouped_batch[1] -= order_product.count_in_units.to_f

                  else

                    r = c
                    loop do
                      grouped_batch = batch[:auto_groupped].select { |k| k[1] > 0 && k[1] >= order_product.units_per_pack }.min_by(&:second)
                      m = if grouped_batch[1] < r * order_product.units_per_pack
                        (grouped_batch[1] / order_product.units_per_pack).to_i
                      else
                        r
                      end
                      grouped_batch[1] -= (m * order_product.units_per_pack).to_f
                      r -= m
                      break if r == 0
                    end

                  end
                end

                order_product.is_fix_price = batch[:is_fix_price]
                order_product.cost_price   = cost_price

                order_product.calculate_tax
                order_product.calculate_totals

                c -= order_product.count.to_f

                # уменьшить баланс в партии
                batch[:batch_balance] -= order_product.count_in_units.to_f
                batch[:balance_per_packs].each { |bpp|
                  bpp[:balance] = if bpp[:unit_type] == "ProductPack" || !order_product.is_fractionary
                    batch[:auto_groupped].blank? ? (batch[:batch_balance] / bpp[:per_unit]).to_i : batch[:auto_groupped].sum { |gb| (gb[1] / bpp[:per_unit]).to_i }
                  else
                    batch[:auto_groupped].blank? ? batch[:batch_balance] : batch[:auto_groupped].sum { |gb| gb[1] }
                  end
                }
                batch[:can_delete] = true
                break if c == 0
              end
            end
          else
            @order.errors.add("variant - #{item[:variant_id]}", I18n.t("errors.batch_not_found"))
          end
        end
      end
    else
      @order.errors.add("order", I18n.t("errors.blank_order_list"), code: 701)
    end

    @order.variants_total            = @order.order_variants.inject(0) { |s, t| s += (t.marked_for_destruction? ? 0 : t.total) }
    @order.only_tax_total            = @order.order_variants.inject(0) { |s, t| s += (t.marked_for_destruction? ? 0 : t.only_tax_total) }
    @order.total                     = @order.order_variants.inject(0) { |s, t| s += (t.marked_for_destruction? ? 0 : t.total) }
    @order.variants_cost_total       = @order.order_variants.inject(0) { |s, t| s += (t.marked_for_destruction? ? 0 : t.cost_price_total) }
  end

  def calculate_totals
    @order.order_variants.each(&:calculate_sales_commission_total)

    @order.sales_commission_total    = @order.order_variants.inject(0) { |s, t| s += (t.marked_for_destruction? ? 0 : t.sales_commission_total) }
    @order.special_offers_discount   = @order.order_variants.inject(0) { |s, t| s += (t.marked_for_destruction? ? 0 : t.special_offers_discount) }
    @order.special_offers_accruals   = @order.order_variants.inject(0) { |s, t| s += (t.marked_for_destruction? ? 0 : t.special_offers_accruals) }

    check_loyalty_program_conditions

    @order.loyalty_discount          = @order.order_variants.inject(0) { |s, t| s += (t.marked_for_destruction? ? 0 : t.loyalty_discount) }
    @order.loyalty_accruals          = @order.order_variants.inject(0) { |s, t| s += (t.marked_for_destruction? ? 0 : t.loyalty_accruals) }
  end


  def prepare_order_transactions(params, all_errors = true)
    @shop_rates = @shop.get_currency_rate_ratio(Time.current)

    is_debt, debt_sum, loyalty_bonus_payed_sum = false, 0, 0
    default_currency = @shop.default_currency

    params[:payments].each do |payment|

      next if payment[:amount].to_f == 0
      next if payment[:currency_id].blank?
      next if payment[:pay_type_id].blank?

      transaction = @order.transactions.build payment
      transaction.user_id               = @user.id
      transaction.shop_id               = @shop.id
      transaction.cashbox_id            = @trade_point.cashbox_id
      transaction.trade_point_id        = @trade_point.id
      transaction.date                  = Time.current
      transaction.shop_currency_rate_id = @shop_rates[transaction.currency_id][:id]
      transaction.converted_amount      = payment[:amount].to_f * @shop_rates[transaction.currency_id][:ratio]
      transaction.operation_type        = transaction.pay_type.debt_payment? ? "outcome" : "income"

      transaction.skip_order_id_validation!

      if transaction.outcome?
        transaction.transaction_type = :accruing_debt
        if !is_debt
          is_debt                 = true
          debt_sum                = transaction.converted_amount
          transaction.debt_amount = transaction.amount
        else
          @order.errors.add("debt transactions", I18n.t("errors.order_can_contain_only_one_debt"))
          break
        end
      else
        transaction.transaction_type = :payment
      end

      if transaction.pay_type.loyalty_card_payment?
        if !@order.can_pay_by_loyalty_card?
          @order.errors.add(:pay_type_id, I18n.t("errors.cant_use_loyalty_card_for_payment"), code: 717)
        elsif transaction.currency_id != default_currency.id
          @order.errors.add(:currency_id, I18n.t("errors.payment_is_possible_only_in_the_main_currency"), code: 719)
        else
          transaction.loyalty_card_id = @order.loyalty_card_id
          loyalty_bonus_payed_sum += transaction.converted_amount
        end
      end
    end

    if loyalty_bonus_payed_sum > 0 && @loyalty_card.balance < loyalty_bonus_payed_sum
      @order.errors.add(:pay_type_id, I18n.t("errors.loyalty_card_balance_less_then_required"), code: 718)
    end

    if @order.loyalty_program_used?
      pay_types = @order.transactions.map(&:pay_type_id).uniq
      if !@loyalty_program.payment_conditions_valid?(pay_types)
        @order.loyalty_program_status = "conditions_not_passed"
        @order.warnings.add(:loyalty_card_id, I18n.t("warnings.loyalty_program.payment_conditions_not_passed"))
        clear_loyalty_values
      end
    end

    total_sum    = @order.transactions.sum(&:converted_amount).round(2)
    total_to_pay = (@order.total - @order.loyalty_discount - @order.special_offers_discount).round(2)

    if total_sum > total_to_pay

      cash_pay_type_id = PayType.cash_pay_type.id
      cash_amount      = @order.transactions.select { |t| t.pay_type_id == cash_pay_type_id }.sum(&:converted_amount)
      change_payment   = total_sum - total_to_pay

      if change_payment > cash_amount || debt_sum >= change_payment
        if all_errors
          @order.errors.add("order total sum", I18n.t("errors.payments_total_sum_greater_than_order_total"), converted_amount: total_sum, order_total: total_to_pay)
        end
      else

        transaction = @order.transactions.build(currency_id: default_currency.id, pay_type_id: cash_pay_type_id, amount: change_payment,
            shop_id: @shop.id, cashbox_id: @trade_point.cashbox_id, trade_point_id: @trade_point.id, user_id: @user.id,
            date: Time.current, converted_amount: change_payment, operation_type: "outcome", transaction_type: "change_payment")
        transaction.skip_order_id_validation!

        if all_errors
          balance = @trade_point.cashbox.balance(cash_pay_type_id, default_currency.id, transaction.date).first
          balance = balance.incomes - balance.outcomes  if !balance.blank?

          if balance.blank? || balance < transaction.amount
            @order.errors.add("change payment", I18n.t("errors.cashbox_balance_less_then_required"), code: 534, current_balance: balance.to_f)
          end
        end

        @order.change_amount = change_payment.to_f
      end
    end

    # @order.to_pay_amount
    @order.to_pay_amount = (total_to_pay.to_f - total_sum.to_f) > 0 ? (total_to_pay.to_f - total_sum.to_f) : 0

    if total_sum < total_to_pay && all_errors
      @order.errors.add("order total sum", I18n.t("errors.payments_total_sum_less_than_order_total"), converted_amount: total_sum, order_total: total_to_pay)
    end

    if is_debt
      @order.order_type = "debt"
      @order.debt_sum   = debt_sum
    else
      @order.order_type = "payed"
    end
  end

  def prepare_order_debt_pay_transactions(params, all_errors = true)
    @shop_rates = @shop.get_currency_rate_ratio(Time.current)

    cash_pay_type_id = PayType.cash_pay_type.id
    default_currency = @shop.default_currency
    converted_payed_sum, converted_cash_payed_sum, loyalty_bonus_payed_sum = 0, 0, 0
    debt_sum = @debt_transaction.converted_amount

    params[:payments].each do |payment|

      next if payment[:amount].to_f == 0

      transaction                       = @order.transactions.build(payment)
      transaction.user_id               = @user.id
      transaction.shop_id               = @shop.id
      transaction.cashbox_id            = @trade_point.cashbox_id
      transaction.trade_point_id        = @trade_point.id
      transaction.transaction_type      = :debt_payment
      transaction.date                  = Time.current
      transaction.shop_currency_rate_id = @shop_rates[transaction.currency_id][:id]
      transaction.converted_amount      = payment[:amount].to_f * @shop_rates[transaction.currency_id][:ratio]
      transaction.operation_type        = "income"
      transaction.parent_transaction_id = @debt_transaction.id

      transaction.skip_order_id_validation!

      converted_payed_sum      += transaction.converted_amount
      converted_cash_payed_sum += (transaction.pay_type_id == cash_pay_type_id ? transaction.converted_amount : 0)

      if transaction.pay_type.debt_payment?
        @order.errors.add("debt transactions", I18n.t("errors.debt_can_not_be_paid_in_debt"))
        break
      end

      if transaction.pay_type.loyalty_card_payment?
        @order.errors.add(:pay_type_id, I18n.t("errors.cant_use_loyalty_card_for_payment"), code: 717)
        break
      end

      # if transaction.pay_type.loyalty_card_payment?
      #   if !@order.can_pay_by_loyalty_card?
      #     @order.errors.add(:pay_type_id, I18n.t("errors.cant_use_loyalty_card_for_payment"), code: 717)
      #   elsif transaction.currency_id != default_currency.id
      #     @order.errors.add(:currency_id, I18n.t("errors.payment_is_possible_only_in_the_main_currency"), code: 719)
      #   else
      #     transaction.loyalty_card_id = @order.loyalty_card_id
      #     loyalty_bonus_payed_sum += transaction.converted_amount
      #   end
      # end
    end

    # if loyalty_bonus_payed_sum > 0 && @loyalty_card.balance < loyalty_bonus_payed_sum
    #   @order.errors.add(:pay_type_id, I18n.t("errors.loyalty_card_balance_less_then_required"), code: 718)
    # end

    if debt_sum > converted_payed_sum
      @debt_transaction.converted_amount -= converted_payed_sum
      @debt_transaction.amount -= (converted_payed_sum / @shop_rates[@debt_transaction.currency_id][:ratio])
      @order.debt_sum = @debt_transaction.converted_amount

    elsif debt_sum < converted_payed_sum

      change_payment = converted_payed_sum - @debt_transaction.converted_amount
      if change_payment > converted_cash_payed_sum
        @order.errors.add("debt transactions", I18n.t("errors.payments_total_sum_greater_than_debt"), converted_amount: converted_payed_sum, debt_total: @debt_transaction.converted_amount)
      else

        transaction = @order.transactions.build(currency_id: default_currency.id, pay_type_id: cash_pay_type_id, amount: change_payment,
            shop_id: @shop.id, cashbox_id: @trade_point.cashbox_id, trade_point_id: @trade_point.id, user_id: @user.id,
            date: Time.current, converted_amount: change_payment, operation_type: "outcome", transaction_type: "change_payment")
        transaction.skip_order_id_validation!

        converted_payed_sum -= change_payment

        if all_errors
          balance = @trade_point.cashbox.balance(cash_pay_type_id, default_currency.id, transaction.date).first
          balance = balance.incomes - balance.outcomes  if !balance.blank?

          if balance.blank? || balance < transaction.amount
            @order.errors.add("change payment", I18n.t("errors.cashbox_balance_less_then_required"), code: 534, current_balance: balance.to_f)
          end
        end
      end

      @order.change_amount = change_payment
    end

    if debt_sum == converted_payed_sum
      @debt_transaction.converted_amount = 0
      @debt_transaction.amount           = 0
      @order.debt_sum                    = 0
      @order.order_type                  = "payed"
    end

    @order.to_pay_amount = @order.debt_sum
  end

  def order_pay_types
    if !@order.blank? && @order.can_pay_by_loyalty_card?
      @shop.shop_pay_types.active.joins(:pay_type).preload(:pay_type).order("pay_types.created_at asc")
    else
      @shop.shop_pay_types.active.joins(:pay_type).exclude_bonus_pay_type.preload(:pay_type).order("pay_types.created_at asc")
    end
  end

  def debt_order_pay_types
    @shop.shop_pay_types.active.joins(:pay_type).exclude_bonus_pay_type.exclude_debt_pay_type
         .preload(:pay_type).order("pay_types.created_at asc")
  end

  def prepare_order_special_offers
    special_offers = @shop.special_offers.active_offers

    # проверка на карту лояльности
    # проверка по времени действия
    # проверка по дате рождения клиента
    # проверка по сумме счета
    special_offers = special_offers.reject { |s|
      !s.loyalty_program_conditions_valid?(@loyalty_program&.id, @loyalty_program&.loyalty_type) ||
      !s.time_conditions_valid? ||
      !s.client_birthday_conditions_valid?(@loyalty_card&.shop_client&.birthday) ||
      !s.order_amount_conditions_valid?(@order.total)
    }

    if !special_offers.blank?
      # весовые товары считаются как одна акционная позиция
      vc_map = @order.order_variants.map { |d|
        d.marked_for_destruction? ? nil : [d.variant_id, d.category_id, (d.is_fractionary ? 1 : d.count_in_units)]
      }.compact

      # проверка на купленный товар и его количество
      special_offers = special_offers.reject { |s| !s.products_buy_conditions_valid?(vc_map) }

      @order.order_variants.each { |k| k.order_variant_special_offers.each(&:mark_for_destruction) }

      special_offers.each { |so|
        # список акционных товаров
        ids, lmt, mlmt = so.products_provided_conditions(vc_map)
        next if ids.blank?

        pcount, ind, mind, sort_weight = 0, 0, 0, 0

        @order.order_variants.sort_by(&:sort_weight).each { |prod|

          next if prod.marked_for_destruction?

          prod.sort_weight = sort_weight
          sort_weight += 1

          next if !ids.include?(prod.variant_id)
          next if mlmt > 0 && mind >= mlmt
          # количество позиций товара
          pcount = prod.is_fractionary ? 1 : prod.count_in_units

          if lmt > 1

            if pcount > 1
              # количесто акционного товара в позиции
              c = ((pcount + ind) / lmt).to_i
              ind += (pcount - c * lmt)

              if c > 0

                # если установлен лимит на максимальное количество позиций акции
                # количество уменьшается до максимального доступного количества
                c = mlmt - mind  if mlmt > 0 && c + mind > mlmt

                mind += c

                # заменяем упаковку на ед измерения товара
                if prod.unit_type == "ProductPack"
                  prod.unit_type = "Unit"
                  prod.unit_id = prod.variant.product.unit_id
                end

                # копируем товар для создания новой акционной позиции
                so_product = @order.order_variants.build(prod.attributes.except("id", "count", "created_at", "updated_at"))
                # количество товара равно количеству доступного акционного товара
                so_product.count = c

                so_product.convert_to_units
                so_product.calculate_totals

                # в скопированный продукт нужно продублировать уже ранее активированные акции
                prod.order_variant_special_offers.each { |ovso|
                  next if ovso.marked_for_destruction?
                  so_product.order_variant_special_offers.build(ovso.attributes.except("id", "created_at", "updated_at"))
                }

                so_product.special_offer_ids << so.id
                so_product.order_variant_special_offers.build(
                  shop_id:             @shop.id,
                  name:                so.name,
                  special_offer_id:    so.id,
                  special_offer_type:  so.special_offer_type,
                  summation_type:      so.summation_type,
                  percent:             so.special_offer_percent.to_i
                )

                # изначальной позиции в счете изменяем количество
                # отнимает от изначального количества  количество акционного товара
                prod.count = pcount - c
                prod.convert_to_units
                prod.calculate_totals

              end

            else
              mind += pcount
              ind +=  pcount

              if ind == lmt
                ind = 0

                prod.special_offer_ids << so.id
                prod.order_variant_special_offers.build(
                  shop_id:             @shop.id,
                  name:                so.name,
                  special_offer_id:    so.id,
                  special_offer_type:  so.special_offer_type,
                  summation_type:      so.summation_type,
                  percent:             so.special_offer_percent.to_i
                )
              end
            end

          else
            if mlmt > 0 && pcount + mind > mlmt
              c = mlmt - mind # сколько акционных позиций еще доступо

              # заменяем упаковку на ед измерения товара
              if prod.unit_type == "ProductPack"
                prod.unit_type = "Unit"
                prod.unit_id = prod.variant.product.unit_id
              end

              # копируем товар для создания новой позиции
              so_product = @order.order_variants.build(prod.attributes.except("id", "count", "created_at", "updated_at"))
              # количество товара равно количеству товара минус количество доступных акционных позиций
              so_product.count = pcount - c

              so_product.convert_to_units
              so_product.calculate_totals

              # в скопированный продукт нужно продублировать уже ранее активированные акции
              prod.order_variant_special_offers.each { |ovso|
                next if ovso.marked_for_destruction?
                so_product.order_variant_special_offers.build(ovso.attributes.except("id", "created_at", "updated_at"))
              }

              # изначальной позиции в счете изменяем количество и связываем с акцией
              prod.count = c
              prod.convert_to_units
              prod.calculate_totals

            end

            prod.special_offer_ids << so.id
            prod.order_variant_special_offers.build(
              shop_id:             @shop.id,
              name:                so.name,
              special_offer_id:    so.id,
              special_offer_type:  so.special_offer_type,
              summation_type:      so.summation_type,
              percent:             so.special_offer_percent.to_i
            )

            mind += pcount

          end
        }

      }

      # группировка по товарам и действующим на них акциям

      groupped_hash = {}
      groupped_keys = if @stock.is_manual_write_off?
        ["variant_id", "unit_id", "unit_type", "price", "price_list_id", "expiration_date", "batch_object_id", "special_offer_ids"]
      else
        ["variant_id", "unit_id", "unit_type", "price", "price_list_id", "expiration_date", "special_offer_ids"]
      end

      @order.order_variants.each_with_index { |prod, index|
        next if prod.marked_for_destruction?

        key = prod.attributes.values_at(*groupped_keys).join("_")

        if groupped_hash.key?(key)
          @order.order_variants[groupped_hash[key]].count += prod.count

          @order.order_variants[groupped_hash[key]].convert_to_units
          @order.order_variants[groupped_hash[key]].calculate_totals

          prod.mark_for_destruction
        else
          groupped_hash[key] = index
        end
      }

      @order.order_variants.each(&:calculate_special_offer_total)
    end
  end

  def save_order_and_write_off_products!
    @order.save! if !@order.errors.any? && @order.valid? && @order.calculate_order
  end

  def update_and_loyalty_realization!
    @order.loyalty_accrual_realization! if !@order.errors.any? && @order.valid? && @order.save!
  end

  def update_order_and_debt_transaction!
    if !@order.errors.any? && @order.valid?
      @debt_transaction.save!
      @order.save!
    end
  end

  class << self
    def call(*args)
      new(*args).call
    end
  end

  private


  # группируем продукты по
  # manual - variant_id, unit_id, unit_type, price, price_list_id, batch_object_id
  # auto   - variant_id, unit_id, unit_type, price, price_list_id, expiration_date
  def group_order_products(order_products)
    order_products.each { |pr| pr[:price] = pr[:price].to_f; pr[:unit_type] ||= "Unit"; }

    if @stock.is_manual_write_off?
      order_products.group_hashes_by(["variant_id", "unit_id", "unit_type", "price", "price_list_id", "expiration_date", "batch_object_id"])
    else
      order_products.group_hashes_by(["variant_id", "unit_id", "unit_type", "price", "price_list_id", "expiration_date"])
    end
  end

  def check_loyalty_program_conditions
    if @order.loyalty_program_used?

      if !@loyalty_program.time_conditions_valid?
        @order.loyalty_program_status = "conditions_not_passed"
        @order.warnings.add(:loyalty_card_id, I18n.t("warnings.loyalty_program.time_conditions_not_passed"))
      end

      if !@loyalty_program.order_conditions_valid?(@order.total - @order.special_offers_discount)
        @order.loyalty_program_status = "conditions_not_passed"
        @order.warnings.add(:loyalty_card_id, I18n.t("warnings.loyalty_program.order_conditions_not_passed"))
      end

      @order.order_variants.each(&:calculate_loyalty_total) if !@order.conditions_not_passed?
    end
  end

  def clear_loyalty_values
    @order.loyalty_accruals = 0
    @order.loyalty_discount = 0
    @order.order_variants.each do |t|
      t.loyalty_discount_percent = 0
      t.loyalty_discount         = 0
      t.loyalty_bonus_percent    = 0
      t.loyalty_accruals         = 0
    end
  end
end
