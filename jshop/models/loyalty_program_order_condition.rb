# frozen_string_literal: true

# == Schema Information
#
# Table name: loyalty_program_order_conditions
#
#  id                 :uuid             not null, primary key
#  amount             :decimal(, )      default(0.0), not null
#  condition_type     :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  loyalty_program_id :uuid             not null
#  shop_id            :uuid             not null
#
# Indexes
#
#  loyalty_order_condition_index  (shop_id,loyalty_program_id,condition_type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (loyalty_program_id => loyalty_programs.id)
#  fk_rails_...  (shop_id => shops.id)
#

class LoyaltyProgramOrderCondition < ApplicationRecord
  audited

  # gteq не менее суммы
  # lteq не менее суммы

  enum default_values: { amount: 0 }
  enum condition_type: { gteq: 0, lteq: 1 }

  validates :shop_id, :condition_type, :amount, presence: true
  validates_db_uniqueness_of :condition_type, index_name: :loyalty_order_condition_index, scope: [:shop_id, :loyalty_program_id]

  db_belongs_to :shop
  db_belongs_to :loyalty_program


  def condition_valid?(val)
    self.amount == 0 || case self.condition_type
                        when "gteq"
                          self.amount <= val
                        when "lteq"
                          self.amount >= val
                        end
  end
end
