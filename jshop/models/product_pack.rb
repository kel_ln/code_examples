# frozen_string_literal: true

# == Schema Information
#
# Table name: product_packs
#
#  id             :uuid             not null, primary key
#  ancestry       :string
#  ancestry_depth :integer          default(0)
#  count          :decimal(20, 3)   default(0.0), not null
#  count_in_units :decimal(20, 4)   default(0.0), not null
#  is_active      :boolean          default(TRUE), not null
#  name           :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  ancestor_id    :uuid
#  product_id     :uuid             not null
#  shop_id        :uuid             not null
#
# Indexes
#
#  index_product_packs_on_ancestor_id                           (ancestor_id)
#  index_product_packs_on_ancestry                              (ancestry)
#  index_product_packs_on_product_id                            (product_id)
#  index_product_packs_on_shop_id                               (shop_id)
#  index_product_packs_on_shop_id_and_product_id_and_is_active  (shop_id,product_id,is_active)
#  index_product_packs_on_shop_id_and_updated_at                (shop_id,updated_at)
#  product_pack_name_unique_index                               (name,product_id,is_active) UNIQUE WHERE (is_active IS TRUE)
#
# Foreign Keys
#
#  fk_rails_...  (ancestor_id => product_packs.id)
#  fk_rails_...  (product_id => products.id)
#  fk_rails_...  (shop_id => shops.id)
#

class ProductPack < ApplicationRecord
  include Concerns::MobileBroadcast
  audited
  has_ancestry cache_depth: true,
               orphan_strategy: :restrict

  validates :shop_id, :product_id, :name, :count, presence: true
  validates_db_uniqueness_of :name, index_name: :product_pack_name_unique_index, scope: [:product_id, :is_active], where: "is_active is TRUE"
  validates :count, numericality: { other_than: 0 }
  validate  :check_product_not_fractionary

  db_belongs_to :shop
  db_belongs_to :product

  before_save :set_ancestor_id
  before_save :convert_to_units
  after_commit :reindex_variants

  scope :active, -> { where(is_active: true) }

  def get_unit_name
    "#{self.name}(#{self.count_in_units})"
  end

  def self.convert_arrange_to_array(parent, childs)
    { value: "#{parent.id}_ProductPack", text: parent.name, children: childs.map { |r, c| ProductPack.convert_arrange_to_array r, c } }
  end

  private

  def set_ancestor_id
    self.ancestor_id = self.parent_id
  end

  def convert_to_units
    self.count_in_units = if self.has_parent?
      self.count * self.parent.count_in_units
    else
      self.count
    end
  end

  def check_product_not_fractionary
    if self.product.is_fractionary
      errors.add(:product, I18n.t("errors.product_pack_cannot_be_added_to_fractionary_product"), code: 488)
    end
  end

  def reindex_variants
    product.variants.reindex
  end
end
