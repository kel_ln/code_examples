# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id                   :uuid             not null, primary key
#  created_by_moderator :boolean          default(FALSE), not null
#  is_active            :boolean          default(TRUE), not null
#  is_fix_price         :boolean          default(FALSE), not null
#  is_fractionary       :boolean          default(FALSE), not null
#  is_gs1_checked       :boolean          default(FALSE), not null
#  is_import            :boolean          default(FALSE), not null
#  is_published         :boolean          default(TRUE), not null
#  locked_at            :datetime
#  manufacturer         :string
#  name                 :string           not null
#  product_mode         :integer          default("pending"), not null
#  rejection_reason     :string(1000)
#  retail_price         :decimal(20, 2)
#  short_name           :string
#  sku                  :string           not null
#  text                 :text
#  weight_code          :string
#  wholesale_price      :decimal(20, 2)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  category_id          :uuid             not null
#  created_shop_id      :uuid             not null
#  created_user_id      :uuid
#  import_id            :uuid
#  locked_by_user_id    :uuid
#  shop_id              :uuid             not null
#  shop_tax_id          :uuid
#  supplier_import_id   :uuid
#  unit_id              :uuid             not null
#
# Indexes
#
#  index_products_on_category_id                             (category_id)
#  index_products_on_created_at                              (created_at)
#  index_products_on_created_shop_id                         (created_shop_id)
#  index_products_on_created_user_id                         (created_user_id)
#  index_products_on_import_id                               (import_id)
#  index_products_on_is_active_and_shop_id_and_product_mode  (is_active,shop_id,product_mode)
#  index_products_on_locked_by_user_id                       (locked_by_user_id)
#  index_products_on_product_mode                            (product_mode)
#  index_products_on_shop_id                                 (shop_id)
#  index_products_on_shop_id_and_is_active                   (shop_id,is_active) WHERE (is_active = true)
#  index_products_on_shop_id_and_updated_at                  (shop_id,updated_at)
#  product_name_unique_index                                 (name,shop_id,is_active,import_id) UNIQUE WHERE (is_active = true)
#  product_sku_unique_index                                  (sku,shop_id) UNIQUE
#  product_weight_code_unique_index                          (weight_code,shop_id) UNIQUE WHERE (is_fractionary = true)
#  products_import_unique_index                              (import_id,shop_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (created_shop_id => shops.id)
#  fk_rails_...  (created_user_id => users.id)
#  fk_rails_...  (locked_by_user_id => users.id)
#  fk_rails_...  (shop_id => shops.id)
#  fk_rails_...  (shop_tax_id => shop_taxes.id)
#  fk_rails_...  (unit_id => units.id)
#

class Product < ApplicationRecord
  include Concerns::MobileBroadcast, Concerns::SpreeBroadcast
  # searchkick
  audited

  MODERATION_LOCK_TIME = 10.minutes
  MODERATION_RELOCK_DRIFT_TIME = 90.seconds

  enum product_mode: { pending: 0, passed: 1, rejected: 2, processing: 3 }

  validates :shop_id, :name, :unit_id, :category_id, presence: true

  # validates_db_uniqueness_of :sku, index_name: :product_sku_unique_index, scope: :shop_id
  # validates_db_uniqueness_of :weight_code, index_name: :product_weight_code_unique_index, scope: :shop_id, where: "is_fractionary = true"
  validates_db_uniqueness_of :name, index_name: :product_name_unique_index, scope: [:shop_id, :is_active, :import_id], where: "is_active = true"
  validates_db_uniqueness_of :import_id, index_name: :products_import_unique_index, scope: [:shop_id]
  validate :check_is_fractionary_type_not_changed, on: :update
  validates :retail_price, presence: true, if: -> { is_fix_price }
  validates :short_name, length: { maximum: 15 }
  validates :rejection_reason, presence: true, if: -> { rejected? }

  db_belongs_to :shop
  db_belongs_to :unit
  db_belongs_to :category
  db_belongs_to :shop_tax
  belongs_to :created_user, class_name: "User", foreign_key: :created_user_id, optional: true
  db_belongs_to :created_shop, class_name: "Shop", foreign_key: :created_shop_id

  has_many :product_barcodes, autosave: true

  has_many :taggings, foreign_key: :taggable_id
  has_many :tags, through: :taggings
  has_many :product_vendor_codes
  has_many :active_product_vendor_codes, -> { where(product_vendor_codes: { is_active: true }) }, class_name: "ProductVendorCode"
  has_many :active_product_barcodes, -> { where(product_barcodes: { is_active: true }) }, class_name: "ProductBarcode"
  has_many :product_packs
  has_many :active_product_packs, -> { where(product_packs: { is_active: true }) }, class_name: "ProductPack"
  has_many :images, -> { where(imageable_type: "product")  },  foreign_key: :imageable_id
  has_many :variants
  has_one  :default_variant, -> { where(is_default: true) }, class_name: "Variant"
  has_many :product_option_types
  has_many :product_option_type_values, through: :product_option_types
  has_one  :locked_moderator, class_name: "User", foreign_key: :locked_by_user_id

  # before_validation :generate_sku, on: :create
  before_validation :set_created_shop, on: :create
  before_update     :break_import_link_on_delete
  after_create      :create_default_variant
  after_update      :update_default_variant
  after_update      :destroy_linked_barcodes
  after_commit      :reindex_variants
  after_save        :variant_sell_prices_recalculate

  scope :active, -> { where(is_active: true) }
  scope :deleted, -> { where(is_active: false) }
  scope :published, -> { where(is_active: true, is_published: true) }
  scope :verified, -> { where.not(product_mode: :pending) }

  scope :by_query, ->(q) {
    query = "%#{q}%"

    left_outer_joins(:active_product_vendor_codes, :active_product_barcodes)
      .where(Product.arel_table[:name].matches(query)
        .or(Product.arel_table[:sku].matches(query)
        .or(ProductVendorCode.arel_table[:vendor_code].matches(query)
        .or(ProductBarcode.arel_table[:barcode].matches(query))))
      ).distinct
  }

  scope :by_search_query, ->(q, shop_id, only_verified = true) {
    ids = Variant.search_json_by_query(q, shop_id, only_verified).pluck(:product_id).uniq
    where(arel_table[:id].in(ids))
  }

  scope :by_name_search_query, ->(q, shop_id, status = true) {
    ids = Variant.search_json_products_list_by_query(q, shop_id, status).pluck(:product_id).uniq
    where(arel_table[:id].in(ids))
  }

  scope :by_category_tree, ->(category_ids, shop_id) {
    ids = Category.search_json_by_id(category_ids, shop_id).pluck(:subtree_path_ids).flatten.uniq
    where(arel_table[:category_id].in(ids))
  }

  scope :by_categories, ->(category_ids) {
    where(arel_table[:category_id].in(category_ids))
  }

  scope :by_tags, ->(tag_ids) {
    joins(:tags).where(Tag.arel_table[:id].in(tag_ids))
  }

  scope :by_options, ->(product_option_type_id, product_option_type_values) {
    joins(variants: [product_option_type_values: [:product_option_type]])
      .where(Variant.arel_table[:is_active].eq(true)
        .and(ProductOptionType.arel_table[:option_type_id].eq(product_option_type_id)
        .and(ProductOptionTypeValue.arel_table[:name].in(product_option_type_values))))
      .distinct
  }

  scope :index_sort, ->(sort) {
    schema_fields = ["name", "sku"]
    order(sort.convert_to_order_fields!("products", schema_fields))
  }

  scope :with_variants_count, -> {
    query = " Select Count(1) From \"variants\"
      Where \"variants\".is_active = true
        And \"variants\".is_default = false
        And \"variants\".shop_id = \"products\".shop_id
        And \"variants\".product_id = \"products\".id"

    select("\"products\".*, coalesce((#{query}), 0) AS variants_count ")
  }

  def get_short_name
    self.short_name || self.name.truncate(15)
  end

  def generate_variants!
    option_ids = self.product_option_types.active.map { |pot| pot.product_option_type_values.active.ids }

    if !option_ids.empty?
      variants_size = option_ids.inject(1) { |c, n| c * n.size }
      variant_option_type_size = option_ids.size
      m_indexes = Array.new(variant_option_type_size) { |i| [0, option_ids[i].size] }

      variants_size.times do |ind|

        variant = self.variants.create(shop_id: self.shop_id)

        votv = Array.new(variant_option_type_size) { |i| { shop_id: self.shop_id,
                                                           product_option_type_value_id: option_ids[i][ m_indexes[i][0] ] } }

        variant.variant_option_type_values.create(votv)

        j = -1
        while (j >= (-variant_option_type_size)) do
          m_indexes[j][0] += 1
          if m_indexes[j][0] == m_indexes[j][1]
            m_indexes[j][0] = 0
            j -= 1
          else
            break
          end
        end

      end
    end
  end

  def is_published?
    is_active && is_published
  end

  # def regenerate_variants!
  #   current_pot_ids = self.product_option_types.active.ids

  #   active_variant_ids = self.variants.ids
  #   active_potv_ids = VariantOptionTypeValue.active.where(variant_id: active_variant_ids).map(&:product_option_type_value_id).uniq
  #   active_pot_ids  = ProducOptionTypeValue.where(id: active_ptv_ids).map(&:product_option_type_id).uniq

  #   deleted_pot_ids = active_pot_ids - current_pot_ids

  #   deleted_pot_ids.each do |pot_id|
  #     product_ot = ProducOptionType.find_by(id: pot_id)
  #     product_otv_ids = product_ot.product_option_type_values.ids

  #     variant_options_for_delete = VariantOptionTypeValue.where(product_option_type_value_id: product_otv_ids)

  #     variant_options_for_delete.each do |vofd|
  #       vofd.is_active = false
  #       vofd.save
  #     end

  #   end
  # end

  def is_verified?
    self.passed? || self.rejected?
  end

  def is_limited_editing?
    self.passed?
  end

  def is_relock_message_send_time?
    is_locked? && Time.now.utc >= locked_until - MODERATION_RELOCK_DRIFT_TIME
  end

  def is_locked?
    self.locked_at && (Time.now.utc <= self.locked_at.utc + MODERATION_LOCK_TIME)
  end

  def locked_until
    is_locked? ? self.locked_at.utc + MODERATION_LOCK_TIME : nil
  end

  def is_locked_by_user?(user_id)
    is_locked? && (self.locked_by_user_id == user_id)
  end

  def lock!(user_id)
    if self.is_locked_by_user? user_id
      self.locked_at = Time.now.utc + (Time.now.utc - self.locked_at.utc)
    else
      self.locked_at = Time.now.utc
      self.locked_by_user_id = user_id
    end
    self.save

    # message to user
    ProductModerationLockTimeBroadcastJob.set(wait: ((self.locked_until - (Time.now.utc + MODERATION_RELOCK_DRIFT_TIME)).to_f / 60).minutes).perform_later(self.id, self.shop_id)
  end

  def unlock!
    self.locked_at = nil
    self.locked_by_user_id = nil
    self.save
  end

  private

  def reindex_variants
    variants.reindex
  end

  def set_created_shop
    self.created_shop_id ||= self.shop_id
  end

  def create_default_variant
    pr = Product.find_by(id: self.id)
    pr.variants.create(shop_id: pr.shop_id,
                       position: 0,
                       retail_price: pr.retail_price,
                       wholesale_price: pr.wholesale_price,
                       is_default: true)
  end

  def break_import_link_on_delete
    if self.is_active == false
      self.is_import = false
      self.import_id = nil
      self.supplier_import_id = nil
    end
  end

  def destroy_linked_barcodes
    if self.is_active == false
      self.active_product_barcodes.each do |pb|
        pb.is_active = false
        pb.save
      end
    end
  end

  def update_default_variant
    self.default_variant.update(retail_price: self.retail_price, wholesale_price: self.wholesale_price, short_name: self.short_name)
  end

  def check_is_fractionary_type_not_changed
    if self.is_fractionary_changed?
      errors.add(:is_fractionary, I18n.t("errors.fractionary_type_cannot_be_changed"), code: 491)
    end
  end


  def generate_sku
    return if !sku.blank?

    last_product = Product.where(shop_id: self.shop_id).order(created_at: :desc).first
    self.sku = SkuGenerator.create_product_sku last_product.try(:sku)
  end

  def variant_sell_prices_recalculate
    SellPricesCalculateJob.perform_later(self.shop_id, self.id, "Product")
  end
end
