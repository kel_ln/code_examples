# frozen_string_literal: true

# == Schema Information
#
# Table name: product_barcodes
#
#  id           :uuid             not null, primary key
#  barcode      :string           not null
#  barcode_type :string           not null
#  is_active    :boolean          default(TRUE), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  product_id   :uuid             not null
#  shop_id      :uuid             not null
#
# Indexes
#
#  index_product_barcodes_on_product_id                            (product_id)
#  index_product_barcodes_on_shop_id                               (shop_id)
#  index_product_barcodes_on_shop_id_and_product_id_and_is_active  (shop_id,product_id,is_active)
#  index_product_barcodes_on_shop_id_and_updated_at                (shop_id,updated_at)
#  product_barcode_unique_index                                    (barcode,barcode_type,shop_id,is_active) UNIQUE WHERE (is_active = true)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#  fk_rails_...  (shop_id => shops.id)
#

class ProductBarcode < ApplicationRecord
  include Concerns::MobileBroadcast
  include Concerns::BarcodeValidate::Ean13, Concerns::BarcodeValidate::Code128

  audited

  BARCODE_TYPES = %w(ean-13 code-128)

  db_belongs_to :shop
  db_belongs_to :product

  validates :shop_id, :barcode_type, :barcode, presence: true
  validates :barcode_type, inclusion: { in: BARCODE_TYPES }
  validates_db_uniqueness_of :barcode, index_name: :product_barcode_unique_index, scope: [:barcode_type, :shop_id, :is_active], where: "is_active = true"

  before_validation :downcase_barcode_type
  before_validation :generate_barcode, on: :create

  after_commit :reindex_variants

  scope :active, -> { where(is_active: true) }

  def self.international_name(b_type)
    case b_type
    when "ean-13"   then "EAN13"
    when "code-128" then "CODE128"
    end
  end

  private

  def reindex_variants
    product.variants.reindex
  end

  def downcase_barcode_type
    self.barcode_type = self.barcode_type.try(:downcase).try(:strip)
  end

  def generate_barcode
    return if !self.barcode.blank? || self.barcode_type != "ean-13" # || self.product.is_fractionary

    u_ind = 0
    loop do
      n = ActiveRecord::Base.connection.execute("SELECT nextval('product_barcodes_#{self.shop_id.gsub('-', '_')}')")[0]["nextval"]
      self.barcode = "#{self.shop.piece_products_prefix}#{n.to_s.rjust(10, '0')}"
      self.barcode += ean_13_checksum.to_s
      break unless ProductBarcode.find_by(shop_id: self.shop_id, barcode_type: self.barcode_type,
                            barcode: self.barcode, is_active: true)

      u_ind += 1
      break if u_ind > 50
    end
  end
end
