# frozen_string_literal: true

# == Schema Information
#
# Table name: loyalty_programs
#
#  id              :uuid             not null, primary key
#  is_active       :boolean          default(TRUE), not null
#  is_default      :boolean          default(FALSE), not null
#  loyalty_percent :decimal(, )      default(0.0), not null
#  loyalty_type    :integer          default("discount"), not null
#  name            :string           not null
#  status          :integer          default("active"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  shop_id         :uuid             not null
#
# Indexes
#
#  index_loyalty_programs_on_loyalty_type  (loyalty_type)
#  index_loyalty_programs_on_status        (status)
#  loyalty_uniq_name_index                 (name,shop_id,is_active) UNIQUE WHERE (is_active = true)
#
# Foreign Keys
#
#  fk_rails_...  (shop_id => shops.id)
#

class LoyaltyProgram < ApplicationRecord
  audited

  enum status: { active: 0, disabled: 1 }
  enum loyalty_type: { discount: 0, bonus: 1 }

  validates :shop_id, :name, :loyalty_percent, :loyalty_type, :status, presence: true
  validates :loyalty_percent, numericality: { in: 0..100 }
  validates_db_uniqueness_of :name, index_name: :loyalty_uniq_name_index, scope: [:shop_id, :is_active], where: "is_active = TRUE"

  after_commit :update_default_program
  after_update_commit :clear_loyalty_product_from_loyalty_card_series

  db_belongs_to :shop
  has_many :loyalty_program_category_conditions, autosave: true
  has_many :loyalty_program_order_conditions,    autosave: true
  has_many :loyalty_program_pay_conditions,      autosave: true
  has_many :loyalty_program_time_conditions,     autosave: true
  has_many :loyalty_card_series, class_name: "LoyaltyCardSerie"

  scope :active, -> {
    where(arel_table[:is_active].eq(true))
  }

  scope :activated, -> {
    where(arel_table[:status].eq("active"))
  }

  scope :by_status, ->(status) {
    where(arel_table[:status].eq(status))
  }

  scope :by_loyalty_type, ->(loyalty_type) {
    where(arel_table[:loyalty_type].eq(loyalty_type))
  }

  scope :by_query_or_ids, ->(q, ids) {
    query = "%#{q}%"

    where(arel_table[:name].matches(query)
      .or(arel_table[:id].in(ids)
    ))
  }

  def time_conditions_valid?
    t = self.loyalty_program_time_conditions.find_by(week_day: Time.current.wday)
    t.present? && Time.current.between?(Time.zone.parse(t.start_at.strftime("%H:%M")), Time.zone.parse(t.end_at.strftime("%H:%M")))
  end

  def order_conditions_valid?(order_amount)
    t = self.loyalty_program_order_conditions
    t.blank? || t.each { |m| break if !m.condition_valid?(order_amount) }.present?
  end

  def payment_conditions_valid?(pay_types)
    t = availible_pay_types
    !t.blank? && t.each { |m| break if ((m + pay_types - (m & pay_types)) - m).blank? }.nil?
  end

  def availible_pay_types
    t = self.loyalty_program_pay_conditions.pluck(:pay_type_id, :condition_type)
    v = t.select { |m| m[1] == "full" }.map { |m| [m[0]] }
    v += [t.select { |m| m[1] == "partial" }.map { |m| m[0] }]
    v.reject(&:empty?)
  end

  private

  def clear_loyalty_product_from_loyalty_card_series
    if !self.is_active
      self.loyalty_card_series.active.find_each { |lp| lp.loyalty_program_id = nil; lp.save }
    end
  end

  def update_default_program
    if self.is_default
      LoyaltyProgram.where(shop_id: self.shop_id, is_default: true)
                .where.not(id: self.id).update_all(is_default: false)
    end
  end
end
