# frozen_string_literal: true

class V1::Shops::ProductsController < V1::ShopApiController
  respond_to :json
  before_action :set_company, :set_shop
  around_action :company_shard

  before_action :set_mapping_settings, only: [:index, :show]

  before_action :read_products,  only: [:index, :search, :show, :export, :list, :list_search,
      :prepare_price_tag_export, :price_tag_export, :prepare_price_tags_export, :price_tags_export]
  before_action :write_products, only: [:create, :update, :destroy_products, :catalog_search, :catalog_product]

  before_action :find_product, only: [:show, :update, :prepare_price_tag_export, :price_tag_export]
  before_action :find_default_shop, only: [:catalog_product, :create]

  def index
    sort = if params[:sort].blank?
      "+name"
    else
      "#{params[:asc].to_i == 1 ? "+" : "-"}#{params[:sort]}"
    end
    page     = params[:page] || 1
    per_page = params[:per_page] || 50

    @products = @current_shop.products.active.includes(:tags, :category, :unit, :active_product_barcodes)
    @products = @products.by_search_query(params[:q], @current_shop.id, false) if !params[:q].blank?
    @products = @products.by_category_tree(params[:category_ids].split(","), @current_shop.id) if !params[:category_ids].blank?
    @products = @products.by_tags(params[:tags_ids].split(",")) if !params[:tags_ids].blank?
    @products = @products.by_options(params[:product_option_type_id], params[:product_option_type_values]&.split(",")) if !params[:product_option_type_id].blank?

    @products = @products.index_sort(sort).page(page).per(per_page)
  end


  def list
    sort     = "+name"
    page     = params[:page] || 1
    per_page = params[:per_page] || 50

    category_id = params[:category_id]
    status = params[:status] || "saved"
    deleted_products = status == "deleted"

    product_categories = if deleted_products
      @current_shop.products.deleted.distinct.pluck(:category_id)
    else
      @current_shop.products.active.distinct.pluck(:category_id)
    end

    if !params[:category_ids].blank?
      product_categories = @current_shop.categories.where(id: product_categories).map(&:path_ids).flatten.uniq
      product_categories = product_categories & params[:category_ids].split(",")
    end

    if category_id.blank?
      root_ids = @current_shop.categories.where(id: product_categories).map(&:root_id).uniq
      @categories = @current_shop.categories.where(id: root_ids).with_products_count(!deleted_products).order(name: :asc)
      @products = []
    else
      path_ids = @current_shop.categories.where(id: product_categories).map(&:path_ids).flatten.uniq
      category = @current_shop.categories.find_by(id: category_id)
      @categories = category.children.where(id: path_ids).with_products_count(!deleted_products).order(name: :asc)

      @products = @current_shop.products.with_variants_count
      @products = deleted_products ? @products.deleted : @products.active

      @products = @products.includes(:tags, :category, :unit, :active_product_barcodes)
      @products = @products.by_categories(category.id)

      @products = @products.by_tags(params[:tags_ids].split(",")) if !params[:tags_ids].blank?
      @products = @products.by_options(params[:product_option_type_id], params[:product_option_type_values]&.split(",")) if !params[:product_option_type_id].blank?

      @products = @products.index_sort(sort).page(page).per(per_page)
    end
  end

  def list_search
    status = params[:status] || "saved"
    deleted_products = status == "deleted"

    @products = @current_shop.products.with_variants_count
    @products = @products.includes(:category, :tags, :unit, :active_product_barcodes)
    @products = deleted_products ? @products.deleted : @products.active

    @products = @products.by_name_search_query(params[:q], @current_shop.id, !deleted_products) if !params[:q].blank?
    @products = @products.by_category_tree(params[:category_ids].split(","), @current_shop.id) if !params[:category_ids].blank?

    @products = @products.by_tags(params[:tags_ids].split(",")) unless params[:tags_ids].blank?
    @products = @products.by_options(params[:product_option_type_id], params[:product_option_type_values]) unless params[:product_option_type_id].blank?
    @products = @products.group_by(&:category_id)

    product_categories_ids = @products.map(&:first)
    path_ids = @current_shop.categories.where(id: product_categories_ids).map(&:path_ids).flatten.uniq
    @categories = @current_shop.categories.where(id: path_ids).arrange(order: :name)
  end


  def catalog_search
    @products = V1::Shops::CatalogProductsSearchQuery.call(catalog_search_params,
        published: true, products_shop_id: @current_shop.id, shop_type: @current_shop.shop_type)
  end

  def catalog_product
    @product = @default_shop.products.find_by(id: params[:id])

    render_404(408, _t("errors.product_not_found")) if @product.blank?
  end

  # !!используется в поиске продуктов на странице акций!!
  def search
    @variants = Variant.search_json_by_query(params[:q], @current_shop.id)

    @selected_ids = params[:variant_ids]&.split(",") || []
    @selected_variants = Variant.search_json_by_ids(@selected_ids, @current_shop.id) if !@selected_ids.blank?
  end

  def create
    @product = nil

    if !params[:product][:import_id].blank?
      if @current_shop.products.find_by(is_import: true, import_id: params[:product][:import_id])
        render_422(433, _t("errors.product_already_imported"))
      else
        default_product = @default_shop.products.find_by(id: params[:product][:import_id])

        @product = V1::ImportGeneralProductService.call(@current_shop, @default_shop, default_product,
            shop_tax_id: import_product_params[:shop_tax_id], category_id: import_product_params[:category_id])

        @product.assign_attributes(import_product_params)
      end
    else
      @product = @current_shop.products.build create_product_params
      @product.created_user_id = current_user.id

      ### MODERATION ###
      # @product.product_mode = :rejected
      # @product.rejection_reason = "Moderation disabled"
      ### MODERATION END ###

      barcode_params[:product_barcodes]&.each do |barcode|
        next if barcode[:_destroy]

        @product.product_barcodes.build(barcode_type: barcode[:barcode_type],
                                        barcode:      barcode[:barcode],
                                        shop_id:      @current_shop.id)
      end
    end


    if !@product.blank?
      if @product.save
        @product = @current_shop.products.find_by(id: @product.id)

        # фотографии если создается новый продукт
        if !@product.is_import
          product_image_params[:product_images]&.each do |img|
            next if img[:_destroy]

            image = @current_shop.images.find_by(id: img[:id])
            image.update_attributes(position:       img[:position] || 0,
                                    imageable_id:   @product.id,
                                    imageable_type: @product.class.name.to_s.downcase)
          end
        end

        ### VENDOR CODES ###
        vendor_code_params[:product_vendor_codes]&.each do |vcode|
          vc = @product.product_vendor_codes.where(vendor_code: vcode[:vendor_code], shop_id: @current_shop.id).first_or_initialize
          vc.is_active = !(!vcode[:_destroy].blank? && vcode[:_destroy])
          vc.save
        end
        ### VENDOR CODES END ###


        ### TAGS ###
        tg = tags_params[:product_tags]&.pluck(:id) || []
        ct = Tag.active.where(shop_id: @current_shop.id, id: tg).pluck(:name)
        dt = Tag.active.where(shop_id: @default_shop.id, id: tg).pluck(:name)
        tags = (ct + dt).uniq
        tg_current = @product.tags.pluck(:name)

        rt = @product.tags.where(name: (tg_current - tags)).pluck(:id)
        @product.taggings.where(tag_id: rt).delete_all

        Tag.active.where(shop_id: @current_shop.id, name: (tags - tg_current)).each do |tag|
          @product.taggings.create(tag_id: tag[:id], shop_id: @current_shop.id)
        end
        ### TAGS END ###


        # ### ДЛЯ ВЕСОВЫХ ТОВАРОВ НЕ ДОСТУПНЫ УПАКОВКИ, СВОЙСТВА И ВАРИАНТЫ ####
        if !@product.is_fractionary

          ### PACKS ####
          packs = pack_params[:product_packs]&.pluck(:name) || []
          pc_current = @product.product_packs.pluck(:name)

          @product.product_packs.where(name: (pc_current - packs)).update_all(is_active: false)

          pack_params[:product_packs]&.each do |pack|
            pp = @product.product_packs.where(name: pack[:name], shop_id: @current_shop.id).first_or_initialize
            pp.count  = pack[:count].to_f
            pp.parent = @product.product_packs.find_by(name: pack[:parent_name])
            pp.save
          end
          ### END PACKS ####


          ### OPTION TYPES ###
          active_pot = []

          product_option_type_params[:product_option_types]&.each do |option_type|
            next if option_type[:id].blank? || option_type[:_destroy]

            opt = OptionType.find_by(shop_id: @current_shop.id, id: option_type[:id])
            if opt.blank?
              opt = OptionType.find_by(shop_id: @default_shop.id, id: option_type[:id])
              opt = OptionType.find_by(shop_id: @current_shop.id, name: opt[:name])
            end

            pot = @product.product_option_types.where(option_type_id: opt.id,
                                                      shop_id:        @current_shop.id).first_or_create

            active_pot << pot.id

            option_type[:product_option_type_values]&.each do |value|
              potv = pot.product_option_type_values.active.where(name: value[:name], shop_id: @current_shop.id).first_or_initialize
              potv.is_active = !(!value[:_destroy].blank? && value[:_destroy])
              potv.save
            end
          end

          @product.product_option_types.active.where.not(id: active_pot).each do |pot|
            pot.is_active = false
            pot.save

            pot.product_option_type_values.update_all(is_active: false)
          end
          ### END OPTION TYPES ###


          ### VARIANTS ###
          active_variants, pot_map = [], {}
          @product.variants.active.map { |b| pot_map[b.id] = b.variant_option_type_values.pluck(:product_option_type_value_id) }

          product_variant_params[:product_variants]&.each do |variant|
            next if variant[:_destroy]

            v_map = variant[:variant_option_type_values].map do |value|
              opt = OptionType.find_by(shop_id: @current_shop.id, id: value[:option_type_id])
              if opt.blank?
                opt = OptionType.find_by(shop_id: @default_shop.id, id: value[:option_type_id])
                opt = OptionType.find_by(shop_id: @current_shop.id, name: opt[:name])
              end
              pot = @product.product_option_types.find_by(option_type_id: opt.id, shop_id: @current_shop.id)
              potv = pot.product_option_type_values.find_by(name: value[:name])

              potv.id
            end

            v_id = pot_map.detect { |k, v| (v - v_map).blank? }&.first

            v_params = { shop_id:         @current_shop.id,
                         vendor_code:     variant[:vendor_code],
                         position:        variant[:position] || 0,
                         short_name:      variant[:short_name],
                         is_published:    variant[:is_published],
                         retail_price:    variant[:retail_price] || @product.retail_price,
                         wholesale_price: variant[:wholesale_price]
                        }

            var = nil
            if v_id.blank?
              var = @product.variants.create(v_params)
              v_map.each do |potv_id|
                var.variant_option_type_values.create(shop_id: @current_shop.id, product_option_type_value_id: potv_id)
              end
            else
              var = @product.variants.find_by(id: v_id)
              var.update(v_params)
            end

            variant[:images].each do |img|
              image = @current_shop.images.find_by(id: img[:id])
              next if image.blank?
              image.update(imageable_id:   var.id,
                           imageable_type: var.class.name.to_s.downcase,
                           position:       img[:position] || 0)

            end
            active_variants << var.id
          end

          @product.variants.active.where.not(id: active_variants).each do |variant|
            variant.is_active = false
            variant.save
          end
          ### END VARIANTS ###
        end

      end
      render status: http_status(@product)
    end
  end

  def show
    @product_tags         = @product.tags.active
    @category_path        = @product.category.path.with_subcategory_count.arrange(order: :name)
    @product_vendor_codes = @product.product_vendor_codes.active
    @product_barcodes     = @product.product_barcodes.active
    @product_packs        = @product.product_packs.active
    @product_images       = @product.images.active.order(position: :desc)
    @product_option_types = @product.product_option_types.active.includes(:product_option_type_values)
    @product_variants     = @product.variants.active.order(position: :desc).includes(:images, variant_option_type_values: :product_option_type_value)
  end

  def update
    if !@product.is_verified?
      render_422(1052, _t("errors.product_not_verified_cant_edit"))
    else

      update_params = @product.is_limited_editing? ? product_update_limited_editing_params : product_update_params

      @product.assign_attributes(update_params)

      if !tags_params[:product_tags].blank?
        product_tags = @product.taggings.pluck(:tag_id)
        active_tags = tags_params[:product_tags].pluck(:id)

        new_tags = active_tags - product_tags
        tags_to_remove = product_tags - active_tags

        # удаление старых тегов
        @product.taggings.where(tag_id: tags_to_remove, shop_id: @current_shop.id).delete_all
        # добавление новых тегов
        new_tags.each do |tid|
          @product.taggings.create(tag_id: tid, shop_id: @current_shop.id)
        end
      else
        @product.taggings.destroy_all
      end

      if !vendor_code_params[:product_vendor_codes].blank?
        vendor_codes = @product.product_vendor_codes.active
        vendor_code_params[:product_vendor_codes].each do |vcode|
          if !vcode[:id].blank?
            vc = vendor_codes.detect { |v| v.id == vcode[:id] }
            if !vc.blank?
              vc_params = vcode.except(:id, :_destroy)
              vc_params[:is_active] = !!!vcode[:_destroy]
              vc.update_attributes(vc_params)
            end
          else
            @product.product_vendor_codes.create(vendor_code: vcode[:vendor_code],
                                                  shop_id: @current_shop.id)
          end
        end
      end

      if !@product.is_limited_editing?
        if !barcode_params[:product_barcodes].blank?
          barcode_codes = @product.product_barcodes.active
          barcode_params[:product_barcodes].each do |barcode|
            if !barcode[:id].blank?
              bc = barcode_codes.detect { |v| v.id == barcode[:id] }
              if !bc.blank?
                bc_params = barcode.except(:id, :_destroy)
                bc_params[:is_active] = !!!barcode[:_destroy]
                bc.update_attributes(bc_params)
              end
            else
              @product.product_barcodes.create(barcode_type: barcode[:barcode_type],
                                                barcode: barcode[:barcode],
                                                shop_id: @current_shop.id)
            end
          end
        end
      end

      if !pack_params[:product_packs].blank?

        current_product_packs_ids = @product.product_packs.active.ids
        active_product_packs_ids = pack_params[:product_packs].pluck(:id)
        ids_for_destroy = current_product_packs_ids - active_product_packs_ids
        packs_for_destroy_ids = @product.product_packs.active.where(id: ids_for_destroy).map(&:subtree_ids).flatten.uniq
        @product.product_packs.where(id: packs_for_destroy_ids).update_all(is_active: false)

        pack_params[:product_packs].each do |pack|
          if !pack[:id].blank? && pack[:id][0] == "_"
            pp = @product.product_packs.build(name: pack[:name],
                                         count: pack[:count],
                                         shop_id: @current_shop.id)
            if !pack[:parent_name].blank?
              parent_pack = @product.product_packs.active.find_by(name: pack[:parent_name])
              pp.parent_id = parent_pack.try(:id)
            end
            pp.save
          else
            # update disabled
          end
        end
      else
        @product.product_packs.active.update_all(is_active: false)
      end

      if !@product.is_limited_editing?
        if !product_image_params[:product_images].blank?
          product_image_params[:product_images].each do |img|
            image = @current_shop.images.find_by(id: img[:id])

            if !image.blank?
              if !!img[:_destroy]
                image.image.destroy
                image.destroy
              else
                img_params = img.except(:id, :_destroy)
                img_params[:imageable_id] = @product.id
                img_params[:imageable_type] = @product.class.name.to_s.downcase
                image.update_attributes(img_params)
              end
            end
          end
        end
      end

      if !product_option_type_params[:product_option_types].blank?

        product_option_type_params[:product_option_types].each do |option_type|

          next if option_type[:id].blank?

          product_option_type = @product.product_option_types.active.where(option_type_id: option_type[:id],
                                                                              shop_id: @current_shop.id).first_or_create
          if !!option_type[:_destroy]

            @product.product_option_types.active
              .where(option_type_id: product_option_type.option_type_id, shop_id: @current_shop.id)
              .update_all(is_active: false)

            @product.product_option_type_values.active
              .where(product_option_types: { option_type_id: product_option_type.option_type_id },  shop_id: @current_shop.id)
              .update_all(is_active: false)
          else

            if !option_type[:product_option_type_values].blank?
              option_type[:product_option_type_values].each do |option_type_value|

                potv = product_option_type.product_option_type_values.where(name: option_type_value[:name],
                                                                         shop_id: @current_shop.id).first_or_initialize
                potv.is_active = !!!option_type_value[:_destroy]
                potv.save
              end
            end

          end

        end
      end

      if !product_variant_params[:product_variants].blank?
        product_variant_params[:product_variants].each do |p_variant|

          pv_params = p_variant.except(:id, :_destroy, :images, :variant_option_type_values)
          pv_params[:retail_price] = pv_params[:retail_price].blank? ? @product.retail_price : pv_params[:retail_price]

          variant = nil

          if !@product.is_import
            if !p_variant[:id].blank?
              variant = @product.variants.active.find_by(id: p_variant[:id])
              if !variant.blank?
                variant.is_active = !!!p_variant[:_destroy]
                variant.update_attributes(pv_params)
              end
            else
              pv_params[:shop_id] = @current_shop.id
              pv_params[:position] ||= 0
              variant = @product.variants.create(pv_params)
            end
          else
            if !p_variant[:id].blank?
              variant = @product.variants.active.find_by(id: p_variant[:id])
              if !variant.blank?
                variant.update_attributes(pv_params)
              end
            end
          end

          if !variant.blank?
            if !p_variant[:images].blank?
              p_variant[:images].each do |img|
                image = @current_shop.images.find_by(id: img[:id])
                if !!img[:_destroy]
                  image.image.destroy
                  image.destroy
                else
                  img_params = img.except(:id, :_destroy)
                  img_params[:imageable_id] = variant.id
                  img_params[:imageable_type] = variant.class.name.to_s.downcase
                  image.update_attributes(img_params)
                end
              end
            end

            # cant modify if import product
            if !@product.is_import
              if !p_variant[:variant_option_type_values].blank?
                product_option_types = @product.product_option_types.active.includes(:product_option_type_values)

                p_variant[:variant_option_type_values].each do |otv|

                  pot = product_option_types.detect { |t| t.option_type_id == otv[:option_type_id] }
                  potv = pot.product_option_type_values.active.detect { |t| !otv[:id].blank? ? t.id == otv[:id] : t.name == otv[:name] }  if !pot.blank?

                  if !potv.blank?
                    votv = variant.variant_option_type_values.active.where(shop_id: @current_shop.id,
                                                                            product_option_type_value_id: potv[:id]).first_or_initialize
                    votv[:is_active] = !!!otv[:_destroy]
                    votv.save
                  end
                end
              end
            end
          end
        end
      end

      @product.save

      render status: http_status(@product)
    end
  end

  def destroy_products
    if !params[:product_ids].blank?
      products = @current_shop.products.active.verified.where(id: params[:product_ids])

      products.each do |product|
        product.is_active = false
        product.save
      end
    end

    render_204
  end


  def export
    status = params[:status] || "saved"

    @products = @current_shop.products.includes(:category, :tags, :unit, :shop_tax, :active_product_barcodes)
    @products = status == "deleted" ? @products.deleted : @products.active

    @products = @products.by_search_query(params[:q], @current_shop.id, false) if !params[:q].blank?
    @products = @products.by_category_tree(params[:category_ids].split(","), @current_shop.id) if !params[:category_ids].blank?

    @products = @products.by_tags(params[:tags_ids].split(",")) unless params[:tags_ids].blank?
    @products = @products.by_options(params[:product_option_type_id], params[:product_option_type_values]) unless params[:product_option_type_id].blank?

    send_data V1::ExcelExport::Products.call(@products, [], params[:columns]), type: "application/xlsx", status: 200
  end


  # export price tag
  def prepare_price_tag_export
    @paper_formats = Paper.all
    @price_tag_templates = @current_shop.price_tag_templates.active.includes(:paper_tag).order(created_at: :desc)

    @product_barcodes = @product.active_product_barcodes.group_by(&:barcode_type)
    @product_variants = @product.variants.all_active.includes(variant_option_type_values: :product_option_type_value)

    variants_ids = @product_variants.pluck(:id)
    user_stocks  = @current_shop.stocks.active.by_user(current_user.id)
    stock_prices = []
    user_stocks.each do |stock|
      stock_prices << Variant.search_sell_products_batches(@current_shop.id, variants_ids, stock.id, stock.write_off_type, current_user.id).map(&:last)
    end
    stock_prices    = stock_prices.flatten.map { |k| k.to_h.slice(:variant_id, :sell_price, :expiration_date) }.uniq
    @variant_prices = stock_prices.inject({}) { |h, k| (h[k[:variant_id]] ||= []) << k; h }
  end


  def price_tag_export
    @variant            = @product.variants.all_active.find_by(id: params[:variant_id])                         if !params[:variant_id].blank?
    @paper              = Paper.find_by(id: params[:paper_format_id])                                           if !params[:paper_format_id].blank?
    @price_tag_template = @current_shop.price_tag_templates.active.find_by(id: params[:price_tag_template_id])  if !params[:price_tag_template_id].blank?
    @product_barcode    = @product.active_product_barcodes.find_by(barcode: params[:barcode])                   if !params[:barcode].blank?
    @currency           = @current_shop.default_currency
    @pages_count        = params[:pages_count] || 1

    if @variant.blank?
      render_404(673, _t("errors.price_tag_export_variant_not_found"))
    elsif @paper.blank?
      render_404(671, _t("errors.price_tag_export_paper_format_not_found"))
    elsif @price_tag_template.blank?
      render_404(672, _t("errors.price_tag_export_price_tag_template_not_found"))
    else
      send_data V1::PriceTagExport::PriceTagExcel.call(variant: @variant, paper: @paper, price_tag_template: @price_tag_template,
        product_barcode: @product_barcode, shop_name: @current_shop.name, price: params[:price], currency: @currency,
        expiration_date: params[:expiration_date], pages_count: @pages_count.to_i),
        type: "application/xlsx", status: 200
    end
  end


  # export price tags
  def prepare_price_tags_export
    @paper_formats = Paper.all
    @price_tag_templates = @current_shop.price_tag_templates.active.includes(:paper_tag).order(created_at: :desc)
  end

  def price_tags_export
    @products           = @current_shop.products.active.where(id: params[:product_ids]&.split(","))                        if !params[:product_ids].blank?
    @paper              = Paper.find_by(id: params[:paper_format_id])                                          if !params[:paper_format_id].blank?
    @price_tag_template = @current_shop.price_tag_templates.active.find_by(id: params[:price_tag_template_id]) if !params[:price_tag_template_id].blank?

    if @products.blank?
      render_404(670, _t("errors.price_tag_export_products_not_found"))
    elsif @paper.blank?
      render_404(671, _t("errors.price_tag_export_paper_format_not_found"))
    elsif @price_tag_template.blank?
      render_404(672, _t("errors.price_tag_export_price_tag_template_not_found"))
    else
      send_data V1::PriceTagExport::PriceTagsExcel.call(products: @products, paper: @paper, price_tag_template: @price_tag_template, shop_name: @current_shop.name),
                         type: "application/xlsx", status: 200
    end
  end


  private

  def find_product
    @product = @current_shop.products.active.find_by(id: params[:id])
    render_404(408, _t("errors.product_not_found")) if @product.blank?
  end

  def find_default_shop
    @default_shop = Shop.active.default.find_by(shop_type: @current_shop.shop_type)
  end

  def catalog_search_params
    params[:product].merge!(page: params[:page])
    params[:product].merge!(per_page: params[:per_page])
    params.require(:product).permit(:page, :per_page, :name, barcodes: [:barcode_type, :barcode])
  end

  def import_product_params
    params.require(:product).permit(:shop_tax_id, :category_id, :manufacturer,
      :is_published, :is_fix_price, :retail_price, :wholesale_price, :short_name
    )
  end

  def create_product_params
    params.require(:product).permit(:name, :text, :unit_id, :shop_tax_id, :category_id, :manufacturer,
      :is_published, :is_fractionary, :is_fix_price, :retail_price, :wholesale_price, :short_name)
  end

  def product_update_params
    params.require(:product).permit(:name, :text, :unit_id, :shop_tax_id, :category_id, :manufacturer,
      :is_published, :is_fix_price, :retail_price, :wholesale_price, :short_name)
  end

  def product_update_limited_editing_params
    params.require(:product).permit(:shop_tax_id, :category_id, :manufacturer,
      :is_published, :is_fix_price, :retail_price, :wholesale_price, :short_name)
  end

  def tags_params
    params.permit(product_tags: [:id])
  end

  def vendor_code_params
    params.permit(product_vendor_codes: [:id, :vendor_code, :_destroy])
  end

  def barcode_params
    params.permit(product_barcodes: [:id, :barcode_type, :barcode, :_destroy])
  end

  def pack_params
    params.permit(product_packs: [:id, :name, :count, :parent_name])
  end

  def product_image_params
    params.permit(product_images: [:id, :position, :_destroy])
  end

  def product_option_type_params
    params.permit(product_option_types: [:id, :_destroy, product_option_type_values: [:name, :_destroy]])
  end

  def product_variant_params
    params.permit(product_variants: [:id, :vendor_code, :position, :short_name,
                  :is_published, :_destroy, :retail_price, :wholesale_price,
                  images: [:id, :position, :_destroy],
                  variant_option_type_values: [:id, :_destroy, :name, :option_type_id]])
  end
end
