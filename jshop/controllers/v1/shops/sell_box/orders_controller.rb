# frozen_string_literal: true

class V1::Shops::SellBox::OrdersController < V1::ShopApiController
  respond_to :json

  before_action :set_company, :set_shop
  around_action :company_shard

  before_action :read_sell_box,  only: [:index, :show]
  before_action :write_sell_box, only: [:create, :update]

  before_action :find_order, only: [:show, :update]
  before_action :find_trade_point, only: [:index, :create, :update]

  def index
    sort = params[:sort] || "created_at"
    sort += (params[:asc].to_i == 1 ? " asc" : " desc")
    status = (["drafted", "debt"].include?(params[:status]) ? params[:status] : "drafted")

    per_page = params[:per_page] || 50

    if status == "drafted"
      status = ["drafted", "saved"]
    end

    @orders = @current_shop.orders.by_status(status).includes(:stock, :cashbox, :trade_point, :user)
    @orders = @orders.by_trade_point(@trade_point.id)
    @orders = @orders.by_date(params[:date]) if !params[:date].blank?

    @orders = @orders.order(sort).page(params[:page]).per(per_page)
  end

  def create
    order = V1::OrderService.new(@current_shop, @trade_point, current_user)
    order.build_new_order(order_params)
    order.prepare_order
    order.prepare_order_variants(order_products_params)
    order.prepare_order_special_offers
    order.calculate_totals

    order.save_order_and_write_off_products!

    @order = order.order
    @order_products = @order.order_variants.includes(:order_variant_special_offers, :unit, variant: [:variant_option_type_values, product: [:product_option_type_values]])

    @default_currency = @current_shop.default_currency

    render status: http_status(@order)
  end

  def update
    order = V1::OrderService.new(@current_shop, @trade_point, current_user)
    order.build_draft_order(@order, order_params)
    order.prepare_order
    order.prepare_order_variants(order_products_params)
    order.prepare_order_special_offers
    order.calculate_totals

    order.save_order_and_write_off_products!

    @order = order.order
    @order_products = @order.order_variants.includes(:order_variant_special_offers, :unit, variant: [:variant_option_type_values, product: [:product_option_type_values]])

    @default_currency = @current_shop.default_currency

    render status: http_status(@order)
  end

  def show
    @order_products = @order.order_variants.includes(:order_variant_special_offers, :unit, variant: [:variant_option_type_values, product: [:product_option_type_values]])
    @order_payments = @order.basic_transactions.includes(:currency, shop_pay_type: :pay_type)

    @default_currency = @current_shop.default_currency
  end

  protected

  def find_trade_point
    @trade_point = @current_shop.trade_points.active.by_user(current_user.id).find_by(id: (params[:trade_point_id] || order_params[:trade_point_id]))
    render_422(702, _t("errors.trade_point_not_found_or_user_have_no_access")) if @trade_point.blank?
  end

  def find_order
    @order = @current_shop.orders.find_by(id: params[:id])
    render_404(700, _t("errors.order_not_found")) if @order.blank?
  end

  def order_params
    params.require(:order).permit(:description, :trade_point_id, :shop_assistant_id, :loyalty_card_id, :is_draft)
  end

  def order_products_params
    params.require(:order).permit(order_products: [:variant_id, :unit_id, :unit_type,
        :count, :price, :expiration_date, :price_list_id, :batch_object_id ])
  end
end
