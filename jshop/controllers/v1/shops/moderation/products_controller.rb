# frozen_string_literal: true

class V1::Shops::Moderation::ProductsController < V1::Shops::Moderation::ModerationBaseController
  before_action :set_product,               only: [:publication, :show, :edit, :relock, :reject, :replace, :update]
  before_action :is_product_mode_editable,  only: [:edit, :relock, :update]
  before_action :is_product_locked,         only: [:relock, :reject, :replace, :update]
  before_action :is_product_mode_pending,   only: [:reject, :replace]
  before_action :set_moderator_audit,       only: [:create, :update, :reject, :replace]

  def index
    @products = V1::Shops::ModerationProductsQuery.call(index_params, shop_type: @current_shop.shop_type)

    @page_ratio = 1 + (@products.current_page - 1) * @products.limit_value
  end

  def search
    @products = V1::Shops::CatalogProductsSearchQuery.call(search_params, shop_type: @current_shop.shop_type)

    @page_ratio = 1 + (@products.current_page - 1) * @products.limit_value
  end

  def totals
    @products = V1::Shops::ModerationProductsQuery.new({}, { shop_type: @current_shop.shop_type })
    @pending, @passed, @rejected = @products.totals
  end

  def create
    default_shop_tax = @current_shop.shop_taxes.active.order(value: :asc).first

    @product = @current_shop.products.build product_create_params
    @product.created_user_id      = current_user.id
    @product.created_by_moderator = true
    @product.shop_tax_id          = default_shop_tax.id
    @product.product_mode         = :passed

    product_barcodes_params[:barcodes]&.each do |barcode|
      @product.product_barcodes.build(barcode_type: barcode[:barcode_type],
                                      barcode:      barcode[:barcode],
                                      shop_id:      @current_shop.id)
    end

    if @product.save

      @product = @current_shop.products.find_by(id: @product.id)

      product_tags_params[:tags]&.each do |tag|
        @product.taggings.create(tag_id: tag[:id], shop_id: @current_shop.id)
      end

      product_images_params[:images]&.each do |image|
        img = @current_shop.images.active.find_by(id: image[:id])
        img.update_attributes(position:       image[:position] || 0,
                              imageable_id:   @product.id,
                              imageable_type: @product.class.name.to_s.downcase)
      end

      # ### ДЛЯ ВЕСОВЫХ ТОВАРОВ НЕ ДОСТУПНЫ УПАКОВКИ, СВОЙСТВА И ВАРИАНТЫ ####
      if !@product.is_fractionary
        product_packs_params[:packs]&.each do |pack|
          pp = @product.product_packs.build(name:    pack[:name],
                                            count:   pack[:count].to_f,
                                            shop_id: @current_shop.id)
          if !pack[:parent_name].blank?
            parent_pack = @product.product_packs.find_by(name: pack[:parent_name])
            pp.parent_id = parent_pack&.id
          end
          pp.save
        end

        product_option_types_params[:option_types]&.each do |option_type|
          pot = @product.product_option_types.create(option_type_id: option_type[:id],
                                                     shop_id:        @current_shop.id)
          option_type[:values]&.each do |value|
            pot.product_option_type_values.create(name:    value[:name],
                                                  shop_id: @current_shop.id)
          end
        end

        product_variants_params[:variants]&.each do |variant|
          vr = @product.variants.create(position:     variant[:position] || 0,
                                        is_published: variant[:is_published],
                                        shop_id:      @current_shop.id)

          variant[:images]&.each do |image|
            img = @current_shop.images.active.find_by(id: image[:id])

            img.update_attributes(position:       image[:position] || 0,
                                  imageable_id:   vr.id,
                                  imageable_type: vr.class.name.to_s.downcase)
          end

          variant[:values].each do |value|
            pot  = @product.product_option_types.find_by(option_type_id: value[:option_type_id])
            potv = pot.product_option_type_values.find_by(name: value[:name])

            vr.variant_option_type_values.create(product_option_type_value_id: potv.id,
                                                 shop_id:                      @current_shop.id)
          end

        end
      end

      ### MODERATOR AUDIT ####
      @audit.product_id                 = @product.id
      @audit.associated_product_id      = @product.id
      @audit.associated_product_shop_id = @product.shop_id
      @audit.action                     = :created
      @audit.save!
      ###


      render status: 201, json: { data: { id: @product.id } }
    else
      render_422_result(@product)
    end
  end

  def show
  end

  def edit
    if @product.is_locked? && !@product.is_locked_by_user?(current_user.id)
      render_422(1101, _t("errors.moderation.product_locked_by_another_user"))
    else
      @product.lock!(current_user.id) if !@product.is_locked?
      if @product.shop.is_default
        @category_path = @product.category.path.with_subcategory_count.arrange(order: :name)
      end
    end
  end

  # если товару нужно продлить время блокировки
  def relock
    @product.lock! current_user.id
  end

  def publication
    if @product.passed?
      @product.assign_attributes(product_publication_params)

      if @product.save
        render status: 200, json: { data: { id: @product.id } }
      else
        render_422_result(@product)
      end
    else
      render_422(1103, _t("errors.moderation.change_publication_status_availible_only_for_passed_products"))
    end
  end

  def reject
    @product.assign_attributes(product_reject_params)
    @product.product_mode = :rejected

    if @product.save
      @product.unlock!

      # message to moderator
      ProductModerationUpdateMessageBroadcastJob.perform_later(current_user.id, @current_shop.id, @product.id)

      # message to user
      products_access_role_ids = @product.shop.company.roles.active.by_secure_object_access("products", "write").pluck(:id)
      company_users_ids = @product.shop.company.user_companies.by_role(products_access_role_ids).pluck(:user_id)
      @product.shop.user_shops.only_active_users.by_user(company_users_ids).each do |shop_user|
        FrontNotifyMessage.create(shop_id: @product.shop_id, user_id: shop_user.user_id,
           event_name: "ProductRejectedByModerator",
           data: { product_id: @product.id, product_name: @product.name,
            message: I18n.t("front_notify_messages.product_rejected_by_moderator", product_name: @product.name)  }
        )
      end

      ### MODERATOR AUDIT ####
      @audit.product_id                 = @product.id
      @audit.associated_product_id      = @product.id
      @audit.associated_product_shop_id = @product.shop_id
      @audit.action                     = :rejected
      @audit.save!
      ###

      render_200
    else
      render_422_result(@product)
    end
  end

  def replace
    replace_product = @current_shop.products.active.passed.find_by(id: params[:product][:replace_product_id])

    if replace_product.blank?
      render_404(1106, _t("errors.moderation.replace_product_not_foud"))
    elsif @product.shop.products.active.find_by(is_import: true, import_id: replace_product.id)
      render_422(1107, _t("errors.moderation.product_already_exist_in_shop"))
    elsif @product.shop_id == @current_shop.id
      render_422(1108, _t("errors.moderation.cant_replace_product_created_in_default_shop"))
    else
      @product.is_import    = true
      @product.import_id    = replace_product.id
      @product.product_mode = :processing
      @product.unlock!

      # message to moderator
      ProductModerationUpdateMessageBroadcastJob.perform_later(current_user.id, @current_shop.id, @product.id)

      UpdateProductsFromGeneralCatelogJob.perform_later(replace_product.id, @product.shop_id)

      ### MODERATOR AUDIT ####
      @audit.product_id                 = replace_product.id
      @audit.associated_product_id      = @product.id
      @audit.associated_product_shop_id = @product.shop_id
      @audit.action                     = :replaced
      @audit.save!
      ###

      render_202
    end
  end


  def update
    # если товар из общего каталога  находим этот товар и обновляем
    # если из каталога другого магазина создаем товар в общем каталоге
    # товар создаем или обновлем отталкиваясь от основного товара
    # ###  МОДЕРАТОР НЕ МОЖЕТ ИЗМЕНИТЬ СВОЙСТВО ВЕСОВОЙ ТОВАР ###

    if @product.shop_id == @current_shop.id

      previous_category_name = @product.category.name
      previous_unit_name     = @product.unit.name

      @product.assign_attributes(product_update_params.except(:unit_id))


      @audit.audited_changes[:name]           = [@product.name_was, @product.name]
      @audit.audited_changes[:category_name]  = [previous_category_name, @product.category.name]
      @audit.audited_changes[:unit]           = [previous_unit_name, @product.unit.name]
      @audit.audited_changes[:is_fractionary] = [@product.is_fractionary_was, @product.is_fractionary]
      @audit.audited_changes[:text]           = [@product.text_was, @product.text]


      ### BARCODES
      previous_barcodes, current_barcodes = [], []
      pr_barcodes = @product.product_barcodes.each { |t|
        previous_barcodes << "#{t.barcode}(#{t.barcode_type})" if t.is_active
        t.is_active = false
      }

      product_barcodes_params[:barcodes]&.each do |barcode|
        pb = pr_barcodes.detect { |t| t[:barcode_type] == barcode[:barcode_type] &&
                                     t[:barcode] == barcode[:barcode] }
        if pb.blank?
          pb = @product.product_barcodes.build(barcode_type: barcode[:barcode_type],
                                                barcode:     barcode[:barcode],
                                                shop_id:     @current_shop.id)
        end
        pb.is_active = true
        current_barcodes << "#{pb.barcode}(#{pb.barcode_type})"
      end

      @audit.audited_changes[:barcodes] = [previous_barcodes.join(", "), current_barcodes.join(", ")]

      ### BARCODES END

      if @product.save

        ### TAGS
        previous_tags, current_tags = [], []

        pr_tags = @product.tags.map { |t|
          previous_tags << t.name
          t.id
        }
        active_tags = product_tags_params[:tags]&.pluck(:id) || []

        @product.taggings.where(tag_id: (pr_tags - active_tags)).delete_all

        product_tags_params[:tags]&.each do |tag|
          tagging = @product.taggings.where(tag_id: tag[:id], shop_id: @current_shop.id).first_or_create
          current_tags << tagging.tag.name
        end

        @audit.audited_changes[:tags] = [previous_tags.join(", "), current_tags.join(", ")]

        ### TAGS END


        ### IMAGES
        previous_images, current_images = [], []

        pr_images = @product.images.map { |t|
          previous_images << t.image.url(:main)
          t.id
        }
        active_images = product_images_params[:images]&.pluck(:id) || []

        @product.images.where(id: (pr_images - active_images)).each do |img|
          img.image.destroy
          img.destroy
        end

        product_images_params[:images]&.each do |image|
          img = @current_shop.images.active.find_by(id: image[:id])
          img.update_attributes(position:       image[:position] || 0,
                                imageable_id:   @product.id,
                                imageable_type: @product.class.name.to_s.downcase)
          current_images << img.image.url(:main)
        end

        @audit.audited_changes[:images] = [previous_images.join(", "), current_images.join(", ")]
        ### IMAGES END

        # ### ДЛЯ ВЕСОВЫХ ТОВАРОВ НЕ ДОСТУПНЫ УПАКОВКИ, СВОЙСТВА И ВАРИАНТЫ ####
        if !@product.is_fractionary

          ### PACKS
          previous_packs, current_packs = [], []

          pr_packs = @product.product_packs.each { |t|
            previous_packs << "#{t.name}(#{t.count})" if t.is_active
            t.is_active = false
          }

          product_packs_params[:packs]&.each do |pack|
            parent_pack = @product.product_packs.find_by(name: pack[:parent_name]) if !pack[:parent_name].blank?

            pp = pr_packs.detect { |t| t[:name] == pack[:name] && t[:ancestor_id] == parent_pack&.id }
            if pp.blank?
              pp = @product.product_packs.build(name:    pack[:name],
                                                count:   pack[:count].to_f,
                                                shop_id: @current_shop.id)
            end

            pp.parent_id = parent_pack&.id
            pp.is_active = true
            pp.save if pp.new_record?

            current_packs << "#{pp.name}(#{pp.count})"
          end

          pr_packs.each(&:save)

          @audit.audited_changes[:packs] = [previous_packs.join(", "), current_packs.join(", ")]
          ### PACKS END

          ### OPTION TYPES
          pr_option_types = @product.product_option_types.each { |t| t[:is_active] = false }
          pr_option_type_values = @product.product_option_type_values.each { |t| t[:is_active] = false }

          product_option_types_params[:option_types]&.each do |option_type|
            pot = pr_option_types.detect { |t| t[:option_type_id] == option_type[:id] }
            if pot.blank?
              pot = @product.product_option_types.build(option_type_id: option_type[:id],
                                                        shop_id:        @current_shop.id)
            end

            pot.is_active = true
            pot.save if pot.new_record?

            option_type[:values]&.each do |value|
              potv = pr_option_type_values.detect { |t| t[:product_option_type_id] == pot.id &&
                                                       t[:name] == value[:name] }

              if potv.blank?
                potv = pot.product_option_type_values.build(name:    value[:name],
                                                            shop_id: @current_shop.id)
              end

              potv.is_active = true
              potv.save if potv.new_record?
            end
          end
          pr_option_types.each(&:save)
          pr_option_type_values.each(&:save)
          ### OPTION TYPES END


          ### VARIANTS
          previous_variants, current_variants = [], []
          pr_variants = @product.variants.active.each { |t|
            previous_variants << t.id if t.is_active
            t.is_active = false
          }

          pot_map = {}
          pr_variants.each { |b| pot_map[b.id] = b.variant_option_type_values.joins(:product_option_type_value)
                .pluck("product_option_type_values.product_option_type_id", "product_option_type_values.name") }

          product_variants_params[:variants]&.each do |variant|

            vpot = variant[:values].pluck(:option_type_id, :name)
            v_id = pot_map.detect { |k, v| (v - vpot).blank? }&.first
            vr   = nil

            if v_id.blank?
              vr = @product.variants.create(position:     variant[:position] || 0,
                                            is_published: variant[:is_published],
                                            shop_id:      @current_shop.id)

              variant[:values].each do |value|
                pot  = @product.product_option_types.find_by(option_type_id: value[:option_type_id])
                potv = pot.product_option_type_values.find_by(name: value[:name])

                vr.variant_option_type_values.create(product_option_type_value_id: potv.id,
                                                     shop_id:                      @current_shop.id)
              end

            else
              vr = pr_variants.detect { |t| t.id == v_id }
              vr.assign_attributes(is_active:     true,
                                   position:      variant[:position] || vr.position,
                                   is_published:  variant[:is_published])
            end

            current_variants << vr.id

            vr_images = vr.images.pluck(:id)
            active_images = variant[:images]&.pluck(:id) || []

            vr.images.where(id: (vr_images - active_images)).each do |img|
              img.image.destroy
              img.destroy
            end

            variant[:images]&.each do |image|
              img = @current_shop.images.active.find_by(id: image[:id])

              img.update_attributes(position:       image[:position] || 0,
                                    imageable_id:   vr.id,
                                    imageable_type: vr.class.name.to_s.downcase)
            end
          end
          pr_variants.each(&:save)

          @audit.audited_changes[:variants] = [false, (previous_variants.sort != current_variants.sort)]
          ### VARIANTS END
        end

        @product.product_mode = :passed
        @product.unlock!

        # message to moderator
        ProductModerationUpdateMessageBroadcastJob.perform_later(current_user.id, @current_shop.id, @product.id)

        UpdateProductsFromGeneralCatelogJob.perform_later(@product.id)

        ### MODERATOR AUDIT ####
        @audit.product_id                 = @product.id
        @audit.associated_product_id      = @product.id
        @audit.associated_product_shop_id = @product.shop_id
        @audit.action                     = :updated
        @audit.save!
        ###

        render status: 200, json: { data: { id: @product.id } }
      else
        render_422_result(@product)
      end
    else

      if !@product.pending? && !@product.rejected?
        render_422(1104, _t("errors.moderation.product_with_this_mode_not_editable"))
      else

        default_shop_tax = @current_shop.shop_taxes.active.order(value: :asc).first
        default_category = @current_shop.categories.active.find_by(id: product_update_params[:category_id])

        g_product = @current_shop.products.build(product_update_params.except(:category_id))
        g_product.category_id      = default_category&.id
        g_product.shop_tax_id      = default_shop_tax.id
        g_product.is_fractionary   = @product.is_fractionary
        g_product.product_mode     = :processing
        g_product.created_user_id  = @product.created_user_id
        g_product.created_shop_id  = @product.shop_id



        ### BARCODES
        product_barcodes_params[:barcodes]&.each do |barcode|
          g_product.product_barcodes.build(barcode_type: barcode[:barcode_type],
                                           barcode:      barcode[:barcode],
                                           shop_id:      @current_shop.id)
        end
        ### BARCODES END

        if g_product.save

          g_product = @current_shop.products.find_by(id: g_product.id)

          ### TAGS
          # теги из 2 списков  - 1 теги товара магазина; 2 теги добавленные модератором
          t_ids = product_tags_params[:tags]&.pluck(:id) || []
          if !t_ids.blank?
            Tag.active.where(shop_id: @product.shop_id, id: t_ids).each do |tag|
              tg = @current_shop.tags.where(name: tag[:name]).first_or_create
              g_product.taggings.create(tag_id: tg.id, shop_id: @current_shop.id)
            end
            Tag.active.where(shop_id: @current_shop.id, id: t_ids).each do |tag|
              g_product.taggings.where(tag_id: tag.id, shop_id: @current_shop.id).first_or_create
            end
          end
          ### TAGS END

          ### IMAGES
          img_ids = product_images_params[:images]&.pluck(:id) || []

          if !img_ids.blank?
            Image.active.where(shop_id: @product.shop_id, id: img_ids).each do |image|
              ig = product_images_params[:images].detect { |t| t[:id] == image.id }

              img = @current_shop.images.build(position:       ig[:position] || 0,
                                              imageable_id:   g_product.id,
                                              imageable_type: g_product.class.name.to_s.downcase)
              img.image = image.image
              img.save
            end
            Image.active.where(shop_id: @current_shop.id, id: img_ids).each do |image|
              ig = product_images_params[:images].detect { |t| t[:id] == image.id }

              image.update_attributes(position:       ig[:position] || 0,
                                      imageable_id:   g_product.id,
                                      imageable_type: g_product.class.name.to_s.downcase)
            end
          end
          ### IMAGES END

          # ### ДЛЯ ВЕСОВЫХ ТОВАРОВ НЕ ДОСТУПНЫ УПАКОВКИ, СВОЙСТВА И ВАРИАНТЫ ####
          if !g_product.is_fractionary

            ### PACKS
            product_packs_params[:packs]&.each do |pack|
              pp = g_product.product_packs.build(name:    pack[:name],
                                                 count:   pack[:count].to_f,
                                                 shop_id: @current_shop.id)
              if !pack[:parent_name].blank?
                parent_pack = g_product.product_packs.find_by(name: pack[:parent_name])
                pp.parent_id = parent_pack&.id
              end
              pp.save
            end
            ### PACKS END

            ### OPTION TYPES
            ot_ids = product_option_types_params[:option_types]&.pluck(:id)
            if !ot_ids.blank?
              OptionType.where(shop_id: @product.shop_id, id: ot_ids).each do |option_type|
                ot  = @current_shop.option_types.active.where(name: option_type.name).first_or_create!
                pot = g_product.product_option_types.create(option_type_id: ot.id,
                                                            shop_id:        @current_shop.id)

                option_type[:values]&.each do |value|
                  pot.product_option_type_values.create(name:    value[:name],
                                                        shop_id: @current_shop.id)
                end
              end
              OptionType.where(shop_id: @current_shop.id, id: ot_ids).each do |option_type|
                pot = g_product.product_option_types.create(option_type_id: option_type.id,
                                                            shop_id:        @current_shop.id)
                option_type[:values]&.each do |value|
                  pot.product_option_type_values.create(name:    value[:name],
                                                        shop_id: @current_shop.id)
                end
              end
            end
            ### OPTION TYPES END

            ### VARIANTS
            product_variants_params[:variants]&.each do |variant|

              ot_ids = variant[:values]&.pluck(:option_type_id)

              next if ot_ids.blank?

              vr = g_product.variants.create(position:     variant[:position] || 0,
                                             is_published: variant[:is_published],
                                             shop_id:      @current_shop.id)

              img_ids = variant[:images]&.pluck(:id)
              if !img_ids.blank?
                Image.active.where(shop_id: @product.shop_id, id: img_ids).each do |image|
                  ig = variant[:images].detect { |t| t[:id] == image.id }

                  img = @current_shop.images.build(position:       ig[:position] || 0,
                                                  imageable_id:   vr.id,
                                                  imageable_type: vr.class.name.to_s.downcase)
                  img.image = image.image
                  img.save
                end
                Image.active.where(shop_id: @current_shop.id, id: img_ids).each do |image|
                  ig = variant[:images].detect { |t| t[:id] == image.id }

                  image.update_attributes(position:       ig[:position] || 0,
                                          imageable_id:   vr.id,
                                          imageable_type: vr.class.name.to_s.downcase)
                end
              end

              if !ot_ids.blank?
                OptionType.where(shop_id: @product.shop_id, id: ot_ids).each do |option_type|
                  vv = variant[:values].detect { |t| t[:option_type_id] == option_type.id }

                  ot   = @current_shop.option_types.active.where(name: option_type.name).first_or_create!
                  pot  = g_product.product_option_types.where(option_type_id: ot.id, shop_id: @current_shop.id).first_or_create!
                  potv = pot.product_option_type_values.where(name: vv[:name], shop_id: @current_shop.id).first_or_create!

                  vr.variant_option_type_values.create(product_option_type_value_id: potv.id,
                                                       shop_id:                      @current_shop.id)
                end
                ProductOptionType.where(product_id: g_product.id, shop_id: @current_shop.id, option_type_id: ot_ids).each do |product_option_type|
                  vv = variant[:values].detect { |t| t[:option_type_id] == product_option_type.option_type_id }

                  potv = product_option_type.product_option_type_values.where(name: vv[:name], shop_id: @current_shop.id).first_or_create!

                  vr.variant_option_type_values.create(product_option_type_value_id: potv.id,
                                                       shop_id:                      @current_shop.id)
                end
              end
            end
            ### VARIANTS END
          end

          g_product.product_mode = :passed
          g_product.save

          @product.is_import    = true
          @product.import_id    = g_product.id
          @product.product_mode = :processing
          @product.unlock!

          # message to moderator
          ProductModerationUpdateMessageBroadcastJob.perform_later(current_user.id, @current_shop.id, @product.id)

          UpdateProductsFromGeneralCatelogJob.perform_later(g_product.id, @product.shop_id)

          ### MODERATOR AUDIT ####
          @audit.product_id                 = g_product.id
          @audit.associated_product_id      = @product.id
          @audit.associated_product_shop_id = @product.shop_id
          @audit.action                     = :accepted
          @audit.save!
          ###

          render status: 200, json: { data: { id: g_product.id } }
        else
          render_422_result(g_product)
        end
      end
    end
  end


  private

  def set_product
    @product = Product.active.find_by(id: params[:id])
    render_404(1100, _t("errors.moderation.product_not_found")) if @product.blank?
  end

  def set_moderator_audit
    @audit = @current_shop.moderator_audits.new(user_id: current_user.id, audited_changes: {})
  end

  def is_product_locked
    if !@product.is_locked_by_user?(current_user.id)
      render_422(1102, _t("errors.moderation.product_must_be_locked_before_updating"))
    end
  end

  def is_product_mode_editable
    if @product.processing?
      render_422(1104, _t("errors.moderation.product_with_this_mode_not_editable"))
    end
  end

  def is_product_mode_pending
    if !@product.pending?
      render_422(1105, _t("errors.moderation.only_pending_products_accepted"))
    end
  end

  def index_params
    params.permit(:product_mode, :q, :created_shop_id, :barcode_type, :barcode,
      :created_date_from, :created_date_to, :tag_id, :category_id, :published_status,
      :page, :per_page, :sort, :asc)
  end

  def search_params
    params[:product].merge!(product_id: params[:id])
    params[:product].merge!(page: params[:page])
    params[:product].merge!(per_page: params[:per_page])
    params.require(:product).permit(:product_id, :page, :per_page, :name, barcodes: [:barcode_type, :barcode])
  end

  def product_publication_params
    params.require(:product).permit(:is_published)
  end

  def product_reject_params
    params.require(:product).permit(:rejection_reason)
  end

  def product_replace_params
    params.require(:product).permit(:replace_product_id)
  end

  def product_update_params
    params.require(:product).permit(:name, :category_id, :unit_id, :text)
  end

  def product_create_params
    params.require(:product).permit(:name, :category_id, :unit_id, :is_fractionary, :text)
  end


  def product_tags_params
    params.require(:product).permit(tags: [:id])
  end

  def product_barcodes_params
    params.require(:product).permit(barcodes: [:barcode_type, :barcode])
  end

  def product_packs_params
    params.require(:product).permit(packs: [:name, :count, :parent_name])
  end

  def product_images_params
    params.require(:product).permit(images: [:position, :id])
  end

  def product_option_types_params
    params.require(:product).permit(option_types: [:id, values: [:name]])
  end

  def product_variants_params
    params.require(:product).permit(variants: [:position, :is_published,
                                          images: [:position, :id],
                                          values: [:name, :option_type_id]]
       )
  end
end
