# frozen_string_literal: true

class V1::Shops::WriteOffsController < V1::ShopApiController
  respond_to :json
  before_action :set_company, :set_shop
  around_action :company_shard

  before_action :set_mapping_settings, only: [:index, :new, :show]

  before_action :read_write_offs, only: [:index, :new, :show, :search, :export_index, :export]
  before_action :write_write_offs, only: [:create, :update, :destroy]

  before_action :find_write_off, only: [:show, :update, :destroy, :export]
  before_action :check_readonly_status, only: [:update, :destroy]

  def index
    sort = if params[:sort].blank?
      "+date"
    else
      "#{params[:asc].to_i == 1 ? "+" : "-"}#{params[:sort]}"
    end

    write_off_type = params[:status] || "saved"

    per_page = params[:per_page] || 50

    @write_offs = @current_shop.write_offs.includes(:stock, :user)
    @write_offs = @write_offs.active.only_write_offs
      .by_user_stocks(current_user.id).by_status(write_off_type)

    @write_offs = @write_offs.by_stock(params[:stock_id]) if !params[:stock_id].blank?
    @write_offs = @write_offs.by_user(params[:user_id]) if !params[:user_id].blank?
    @write_offs = @write_offs.by_date(params[:from_date], params[:to_date]) if !params[:from_date].blank? || !params[:to_date].blank?

    @total_cost = @write_offs.sum(&:total_cost)

    @write_offs = @write_offs.index_sort(sort).page(params[:page]).per(per_page)
  end

  def new
  end

  def search
    @date = params[:date].to_datetime.to_local_timezone

    @shop_rates = @current_shop.get_currency_rate_ratio(@date)

    @stock = @current_shop.stocks.active.find_by(id: params[:stock_id])
    if !@stock.blank?

      variants_json = Variant.search_json_by_query(params[:q], @current_shop.id)
      variants_ids = variants_json.pluck(:id).uniq

      @variants = @current_shop.variants.where(id: variants_ids).includes(variant_option_type_values: :product_option_type_value,
                      product: [:unit, :category, :active_product_vendor_codes, :active_product_barcodes, :active_product_packs])
      @variants = @variants.by_positive_balance(@stock.id, @current_shop.id, @date)
      @variants = @variants.limit(50)

      @invoices = Variant.get_active_invoices(@variants.ids, @stock.id, @date)
    else
      render_404(416, _t("errors.stock_not_found"))
    end
  end

  def show
    @date = params[:date]&.to_datetime&.to_local_timezone || @write_off.date

    @write_off_products = @write_off.write_off_products.active
                                    .with_invoice_products_by_date(@date)
      .includes(:unit, invoice_product: [:document],
        variant: [variant_option_type_values: :product_option_type_value,
                   product: [:unit, :category, :active_product_vendor_codes,
                             :active_product_barcodes, :active_product_packs] ])
  end

  def create
    @write_off = @current_shop.write_offs.build(write_off_params)
    @write_off.responsible_user_id = current_user.id

    @shop_rates = @current_shop.get_currency_rate_ratio(@write_off.date)

    wo_object_ids = write_off_product_params[:write_off_products].pluck(:object_product_id)
    wo_objects    = Variant.get_invoices_objects_for_write_off(wo_object_ids, @write_off.stock_id, @write_off.date)

    variants_ids = write_off_product_params[:write_off_products].pluck(:variant_id)
    variants     = @current_shop.variants.all_active.where(id: variants_ids)

    write_off_product_params[:write_off_products].each do |wop|
      variant = variants.detect { |r| r[:id] == wop[:variant_id] }

      if !variant.blank?

        @write_off_product = @write_off.write_off_products.build(wop)
        @write_off_product.convert_to_units

        @wo_object = wo_objects.detect { |r| r[:id] == @write_off_product.object_product_id }

        if @wo_object.blank?
          @write_off.errors.add("write off object", _t("errors.invoice_object_not_found"), code: 418, object_product_id: wop[:object_product_id])
        elsif wop[:count].blank? || @write_off_product.count_in_units <= 0
          @write_off.errors.add("write off object", _t("errors.count_couldnt_be_less_than_1"), code: 419, object_product_id: wop[:object_product_id])
        elsif @wo_object.balance < @write_off_product.count_in_units
          @write_off.errors.add("write off object", _t("errors.balance_less_than_requested_value"),  code: 420, invoice_number: @wo_object.document_number,
             invoice_date: @wo_object.document.document_date, object_product_id: @wo_object.id, balance: @wo_object.balance)
        else

          @write_off_product.document_currency_id  = @wo_object.document.currency_id
          @write_off_product.shop_currency_rate_id = @shop_rates[@write_off_product.document_currency_id][:id]
          @write_off_product.document_price        = @wo_object.per_unit_cost_price
          @write_off_product.purchase_price        = @wo_object.per_unit_cost_price * @shop_rates[@write_off_product.document_currency_id][:ratio]
          @write_off_product.expiration_date       = @wo_object.expiration_date

          @write_off_product.shop_id               = @current_shop.id
          @write_off_product.shop_tax_id           = variant.product.shop_tax_id
          @write_off_product.calculate_amount

        end
      else
        @write_off.errors.add("write off product", _t("errors.variant_not_found"), code: 421, variant_id: wop[:variant_id])
      end
    end

    invoice_products = @write_off.write_off_products.map { |r| [r.variant_id, r.object_product_id] }
    if invoice_products.size - invoice_products.uniq.size > 0
      invoice_products.find_duplicates.each do |val|
        @write_off.errors.add("write off product", _t("errors.broduct_batch_not_unique"), code: 423, variant_id: val[0], object_product_id: val[1])
      end
    end

    @write_off.total_cost = @write_off.write_off_products.sum(&:amount)

    @write_off.write_off_realization if !@write_off.errors.any? && @write_off.valid? && @write_off.save

    render status: http_status(@write_off)
  end

  def update
    update_params = write_off_params.except(:stock_id) # disable change stock

    @write_off.assign_attributes(update_params)
    @write_off.responsible_user_id = current_user.id

    @shop_rates = @current_shop.get_currency_rate_ratio(@write_off.date)

    # new[ variant_id ], updated/deleted [:write_off_product_id]
    changed_params = { new: [], updated: [], deleted: [] }
    @write_off_products = @write_off.write_off_products

    # if @write_off.stock_id_changed?
    #   @write_off_productseach do |wop|
    #     wop.is_active = false
    #     changed_params[:deleted] << wop.id
    #   end
    # end

    @write_off_products_ids = @write_off_products.map { |d| d.is_active ? d.id : nil }.compact
    @active_write_off_products_ids  = write_off_product_params[:write_off_products].pluck(:id)
    @deleted_write_off_products_ids = @write_off_products_ids - @active_write_off_products_ids

    @deleted_write_off_products_ids.each do |deleted_id|
      wof_product = @write_off_products.detect { |w| w.id == deleted_id }

      if !wof_product.blank?
        wof_product.is_active = false
        changed_params[:deleted] << wof_product.id
      else
        @write_off.errors.add("write off object", _t("errors.write_off_object_id_not_found"), code: 422, id: deleted_id)
      end
    end

    variants_ids = write_off_product_params[:write_off_products].pluck(:variant_id)
    variants     = @current_shop.variants.all_active.where(id: variants_ids)

    wo_object_ids = write_off_product_params[:write_off_products].pluck(:object_product_id)
    wo_objects    = Variant.get_invoices_objects_for_write_off(wo_object_ids, @write_off.stock_id, @write_off.date)

    write_off_product_params[:write_off_products].each do |wop|

      if wop[:id].blank?

        variant = variants.detect { |r| r[:id] == wop[:variant_id] }
        if !variant.blank?

          @write_off_product = @write_off.write_off_products.build(wop)
          @write_off_product.convert_to_units

          @wo_object = wo_objects.detect { |r| r[:id] == @write_off_product.object_product_id }

          if @wo_object.blank?
            @write_off.errors.add("write off object", _t("errors.invoice_object_not_found"), code: 418, object_product_id: wop[:object_product_id])
          elsif wop[:count].blank? || @write_off_product.count_in_units <= 0
            @write_off.errors.add("write off object", _t("errors.count_couldnt_be_less_than_1"), code: 419, object_product_id: wop[:object_product_id])
          elsif @wo_object.balance < @write_off_product.count_in_units
            @write_off.errors.add("write off object", _t("errors.balance_less_than_requested_value"),  code: 420, invoice_number: @wo_object.document_number,
                invoice_date: @wo_object.document.document_date, object_product_id: @wo_object.id, balance: @wo_object.balance)
          else

            @write_off_product.document_currency_id  = @wo_object.document.currency_id
            @write_off_product.shop_currency_rate_id = @shop_rates[@write_off_product.document_currency_id][:id]
            @write_off_product.document_price        = @wo_object.per_unit_cost_price
            @write_off_product.purchase_price        = @wo_object.per_unit_cost_price * @shop_rates[@write_off_product.document_currency_id][:ratio]
            @write_off_product.expiration_date       = @wo_object.expiration_date

            @write_off_product.shop_id               = @current_shop.id
            @write_off_product.shop_tax_id           = variant.product.shop_tax_id
            @write_off_product.calculate_amount

            changed_params[:new] << @write_off_product.object_product_id
          end

        else
          @write_off.errors.add("write off product", _t("errors.variant_not_found"), code: 421, variant_id: wop[:variant_id])
        end
      else

        wof_product = @write_off_products.detect { |w| w.id == wop[:id] }

        if !wof_product.blank?

          if wof_product.is_active

            wof_product.count     = wop[:count]     if !wop[:count].blank?
            wof_product.unit_id   = wop[:unit_id]   if !wop[:unit_id].blank?
            wof_product.unit_type = wop[:unit_type] if !wop[:unit_type].blank?
            wof_product.convert_to_units

            if wof_product.count_in_units_changed? || @write_off.date_changed?

              wo_object = wo_objects.detect { |r| r[:id] == wof_product.object_product_id }

              if wo_object.blank?
                @write_off.errors.add("write off object", _t("errors.invoice_object_not_found"), code: 418, object_product_id: wof_product.object_product_id)
              elsif wop[:count].blank? || wof_product.count_in_units <= 0
                @write_off.errors.add("write off object", _t("errors.count_couldnt_be_less_than_1"), code: 419, object_product_id: wof_product.object_product_id)
              elsif wo_object.balance + wof_product.count_in_units_was < wof_product.count_in_units
                @write_off.errors.add("write off object", _t("errors.balance_less_than_requested_value"),  code: 420, invoice_number: wo_object.document_number,
                    invoice_date: wo_object.document.document_date, object_product_id: wo_object.id, balance: wo_object.balance)
              else

                wof_product.document_currency_id  = wo_object.document.currency_id
                wof_product.shop_currency_rate_id = @shop_rates[wof_product.document_currency_id][:id]
                wof_product.document_price        = wo_object.per_unit_cost_price
                wof_product.purchase_price        = wo_object.per_unit_cost_price * @shop_rates[wof_product.document_currency_id][:ratio]
                wof_product.expiration_date       = wo_object.expiration_date

                wof_product.calculate_amount

                changed_params[:updated] << wof_product.id
              end
            end
          end
        else
          @write_off.errors.add("write off object", _t("errors.write_off_object_id_not_found"), code: 422, id: wop[:id])
        end
      end

    end

    invoice_products = @write_off_products.map { |r| r.is_active ? [r.variant_id, r.object_product_id] : nil }.compact
    if invoice_products.size - invoice_products.uniq.size > 0
      invoice_products.find_duplicates.each do |val|
        @write_off.errors.add("write off product", _t("errors.broduct_batch_not_unique"), code: 423, variant_id: val[0], object_product_id: val[1])
      end
    end

    @write_off.total_cost = @write_off_products.inject(0) { |sum, wp| sum += (wp.is_active ? wp.amount : 0) }

    @write_off.write_off_update_realization(changed_params) if !@write_off.errors.any? && @write_off.valid? && @write_off_products.each(&:save!) && @write_off.save!

    render status: http_status(@write_off)
  end

  def destroy
    @write_off.status = "deleted"
    @write_off.write_off_cancellation if @write_off.save

    render_204
  end



  def export_index
    @mapping_table = MappingSetting.where(action_id: "#{controller_path}/index", shop_id: @current_shop.id, user_id: current_user.id).first_or_initialize
    @excluded_columns = @mapping_table.excluded_columns

    write_off_type = params[:status] || "saved"

    @write_offs = @current_shop.write_offs.includes(:stock, :user)
    @write_offs = @write_offs.active.exclude_relocations.exclude_inventories.by_user_stocks(current_user.id).by_status(write_off_type)

    @write_offs = @write_offs.by_stock(params[:stock_id]) if !params[:stock_id].blank?
    @write_offs = @write_offs.by_user(params[:user_id]) if !params[:user_id].blank?
    @write_offs = @write_offs.by_date(params[:from_date], params[:to_date]) if !params[:from_date].blank? || !params[:to_date].blank?

    send_data V1::ExcelExport::WriteOffs.call(@write_offs, @excluded_columns), type: "application/xlsx", status: 200
  end


  def export
    @mapping_table = MappingSetting.where(action_id: "#{controller_path}/index", shop_id: @current_shop.id, user_id: current_user.id).first_or_initialize
    @excluded_columns = @mapping_table.excluded_columns

    @write_off_products = @write_off.write_off_products.active
      .includes(:unit, :invoice_product, variant: [ product: [:product_vendor_codes, :product_barcodes, :product_packs] ])

    send_data V1::ExcelExport::WriteOff.call(@write_off_products, @excluded_columns, @write_off), type: "application/xlsx", status: 200
  end


  protected

  def check_readonly_status
    render_422(424, _t("errors.cant_modify_readonly_document")) if @write_off.is_readonly
  end

  def find_write_off
    @write_off = @current_shop.write_offs.includes(:stock, :user).active.find_by(id: params[:id])
    render_404(417, _t("errors.write_off_not_found")) if @write_off.blank?
  end

  def write_off_params
    params.require(:write_off).permit(:date, :stock_id, :note)
  end

  def write_off_product_params
    params.require(:write_off).permit(write_off_products: [:id, :variant_id,
      :object_product_id, :unit_id, :unit_type, :count])
  end
end
