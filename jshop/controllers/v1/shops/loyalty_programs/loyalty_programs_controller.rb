# frozen_string_literal: true

class V1::Shops::LoyaltyPrograms::LoyaltyProgramsController < V1::ShopApiController
  respond_to :json

  before_action :set_company, :set_shop
  around_action :company_shard

  before_action :set_mapping_settings, only: [:index, :show, :new]

  before_action :read_loyalty_programs, only: [:index, :show, :new, :search]
  before_action :write_loyalty_programs, only: [:create, :update, :destroy]

  before_action :find_loyalty_program, only: [:update, :show, :destroy]


  def index
    sort = "name"
    page = params[:page] || 0
    per_page = params[:per_page] || 50


    @loyalty_programs = @current_shop.loyalty_programs.active
    @loyalty_programs = @loyalty_programs.by_status(params[:status]) if !params[:status].blank?
    @loyalty_programs = @loyalty_programs.by_loyalty_type(params[:loyalty_type]) if !params[:loyalty_type].blank?

    @loyalty_programs = @loyalty_programs.order(sort).page(page).per(per_page)
  end

  def new
    @shop_pay_types = @current_shop.active_shop_pay_types.includes(:pay_type)
  end

  def search
    sort = "name"
    selected_ids = params[:loyalty_program_ids]&.split(",") || []

    @loyalty_programs = @current_shop.loyalty_programs.active
    @loyalty_programs = @loyalty_programs.by_query_or_ids(params[:q], selected_ids)
    @loyalty_programs = @loyalty_programs.order(sort).limit(20 + selected_ids.size)
  end

  def show
    @shop_pay_types = @current_shop.active_shop_pay_types.includes(:pay_type)
  end

  def create
    @loyalty_program = @current_shop.loyalty_programs.build(loyalty_program_params)

    loyalty_program_order_conditions_params[:order_conditions]&.each do |c|
      lc = @loyalty_program.loyalty_program_order_conditions.build(c)
      lc.shop_id = @current_shop.id
    end

    loyalty_program_pay_conditions_params[:pay_conditions]&.each do |c|
      lc = @loyalty_program.loyalty_program_pay_conditions.build(c)
      lc.shop_id = @current_shop.id
    end

    loyalty_program_time_conditions_params[:time_conditions]&.each do |c|
      next if c[:is_active].blank? || c[:is_active] == false
      lc = @loyalty_program.loyalty_program_time_conditions.build(c.except(:is_active))
      lc.shop_id = @current_shop.id
    end

    @loyalty_program.save
    render status: http_status(@loyalty_program)
  end

  def update
    @loyalty_program.assign_attributes(loyalty_program_params)

    if loyalty_program_order_conditions_params[:order_conditions].blank?
      @loyalty_program.loyalty_program_order_conditions.each(&:mark_for_destruction)
    else
      updated = loyalty_program_order_conditions_params[:order_conditions].pluck(:condition_type)
      actual = @loyalty_program.loyalty_program_order_conditions.pluck(:condition_type)
      for_delete = actual - updated
      @loyalty_program.loyalty_program_order_conditions.select { |t| for_delete.include?(t.condition_type) }.each(&:mark_for_destruction)

      loyalty_program_order_conditions_params[:order_conditions].each do |c|
        lc = @loyalty_program.loyalty_program_order_conditions.detect { |t| t.condition_type == c[:condition_type] }
        if lc.blank?
          lc = @loyalty_program.loyalty_program_order_conditions.build(c)
          lc.shop_id = @current_shop.id
        else
          lc.assign_attributes(c)
        end
      end
    end

    if loyalty_program_pay_conditions_params[:pay_conditions].blank?
      @loyalty_program.loyalty_program_pay_conditions.each(&:mark_for_destruction)
    else
      updated = loyalty_program_pay_conditions_params[:pay_conditions].pluck(:pay_type_id)
      actual = @loyalty_program.loyalty_program_pay_conditions.pluck(:pay_type_id)
      for_delete = actual - updated
      @loyalty_program.loyalty_program_pay_conditions.select { |t| for_delete.include?(t.pay_type_id) }.each(&:mark_for_destruction)

      loyalty_program_pay_conditions_params[:pay_conditions].each do |c|
        lc = @loyalty_program.loyalty_program_pay_conditions.detect { |t| t.pay_type_id == c[:pay_type_id] }
        if lc.blank?
          lc = @loyalty_program.loyalty_program_pay_conditions.build(c)
          lc.shop_id = @current_shop.id
        else
          lc.assign_attributes(c)
        end
      end
    end

    if loyalty_program_time_conditions_params[:time_conditions].blank?
      @loyalty_program.loyalty_program_time_conditions.each(&:mark_for_destruction)
    else
      updated = loyalty_program_time_conditions_params[:time_conditions].map { |wd| wd[:is_active].blank? || wd[:is_active] == false ? nil : wd[:week_day] }.compact
      actual = @loyalty_program.loyalty_program_time_conditions.pluck(:week_day)
      for_delete = actual - updated
      @loyalty_program.loyalty_program_time_conditions.select { |t| for_delete.include?(t.week_day) }.each(&:mark_for_destruction)

      loyalty_program_time_conditions_params[:time_conditions].each do |c|
        next if c[:is_active].blank? || c[:is_active] == false
        lc = @loyalty_program.loyalty_program_time_conditions.detect { |t| t.week_day == c[:week_day] }
        val = c.except(:is_active)
        if lc.blank?
          lc = @loyalty_program.loyalty_program_time_conditions.build(val)
          lc.shop_id = @current_shop.id
        else
          lc.assign_attributes(val)
        end
      end
    end

    @loyalty_program.save
    render status: http_status(@loyalty_program)
  end

  def destroy
    @loyalty_program.is_active = false
    @loyalty_program.save

    render_204
  end

  private

  def find_loyalty_program
    @loyalty_program = @current_shop.loyalty_programs.active
          .includes(:loyalty_program_category_conditions, :loyalty_program_order_conditions,
                     :loyalty_program_pay_conditions, :loyalty_program_time_conditions)
          .find_by(id: params[:id])
    render_404(800, _t("errors.loyalty_program_not_found")) if @loyalty_program.blank?
  end

  def loyalty_program_params
    params.require(:loyalty_program).permit(:name, :status, :loyalty_type, :loyalty_percent, :is_default)
  end

  def loyalty_program_order_conditions_params
    params.require(:loyalty_program).permit(order_conditions: [:condition_type, :amount])
  end

  def loyalty_program_pay_conditions_params
    params.require(:loyalty_program).permit(pay_conditions: [:pay_type_id, :condition_type])
  end

  def loyalty_program_time_conditions_params
    params.require(:loyalty_program).permit(time_conditions: [:is_active, :week_day, :start_at, :end_at])
  end
end
