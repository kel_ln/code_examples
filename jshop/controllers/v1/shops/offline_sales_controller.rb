# frozen_string_literal: true

class V1::Shops::OfflineSalesController < V1::ShopApiController
  respond_to :json
  before_action :set_company, :set_shop
  around_action :company_shard

  before_action :set_mapping_settings, only: [:index, :show]

  before_action :read_offline_sales, only: [:index, :show]
  before_action :write_offline_sales, only: [:create, :update]

  before_action :set_params_from_id, only: [:show, :update]
  before_action :set_stock, only: [:index, :show, :create, :update]
  before_action :set_product, only: [:show, :update]
  before_action :set_lock_status, only: [:index, :create, :update]
  before_action :check_lock_status, only: [:create, :update]

  def index
    per_page = params[:per_page] || 50
    page = params[:page] || 0

    from_date = params[:from_date]&.to_date || @current_shop.created_at
    to_date   = params[:to_date]&.to_date   || Time.now

    @from_date = from_date.beginning_of_day
    @to_date   = to_date.end_of_day

    @products = @current_shop.order_variants.only_offline_orders.need_to_write_off# .not_locked
    @products = @products.by_order_stock(@stock.id)
    @products = @products.by_date(@from_date, @to_date)
    @products = @products.group_and_order_for_offline_sales
    @products = @products.preload(:unit, variant: [variant_option_type_values: [:product_option_type_value],
        product: [:shop_tax]])

    @products = @products.page(page).per(per_page)

    if @lock_status
      @job_status = @current_shop.scheduled_jobs.offline_sales.last&.get_status_info
    end
  end

  def show
    per_page = params[:per_page] || 50
    page = params[:page] || 0

    @invoices = @current_shop.invoice_products.active.active_document.positive_balance
                  .by_stock(@stock.id).by_variant(@variant_id)
                  .sort_by_stock_write_off_type(@stock.write_off_type)
                  .page(page).per(per_page)
    @invoices = @invoices.preload(:document)
  end

  # списание в автоматическом режиме не доступно для складов с ручным списанием
  # auto write off
  # счета блокируются
  # создается документ списания
  def create
    @errors = []

    from_date = auto_write_off_params[:from_date]&.to_date || @current_shop.created_at
    to_date   = auto_write_off_params[:to_date]&.to_date   || Time.now

    @from_date = from_date.beginning_of_day
    @to_date   = to_date.end_of_day

    if @stock.is_manual_write_off?
      @errors << { code: 493, title:  _t("errors.automatic_write_off_is_not_available_for_this_stock") }
    end

    if @errors.empty?
      locked_time = Time.now.to_s
      @current_shop.order_variants.only_offline_orders.need_to_write_off.not_locked
          .by_order_stock(@stock.id)
          .by_date(@from_date, @to_date)
          .update_all(locked_to_write_off: true, locked_to_write_off_at: locked_time)

      job = OfflineOrdersAutoWriteOffsJob.perform_later(@current_shop.id, current_user.id, @stock.id, @from_date.as_json, @to_date.as_json, locked_time.as_json)
      @current_shop.scheduled_jobs.offline_sales.create(job_id: job.provider_job_id)
      render_202
    else
      render status: 422, json: { errors: @errors }
    end
  end

  # manual write off
  # приходят партии и количество с каждой партиии для списания
  # количество приходит в упаковках
  # количество блокируется с накладных
  # счета с товарами блокируются для распределения
  # создается документ списания
  def update
    count_from_invoices = manual_write_off_params[:invoices].inject(0) { |s, n| s += n[:count].to_f  }
    return render_422(496, _t("errors.invoices_count_greater_than_orders_count")) if count_from_invoices > @product.count_to_write_off

    invoices_ids = manual_write_off_params[:invoices].pluck(:id)
    invoices = @current_shop.invoice_products.active.active_document.positive_balance
                    .by_stock(@stock.id).where(id: invoices_ids)

    manual_write_off_params[:invoices].each do |invoice|
      inv = invoices.detect { |n| n.id == invoice[:id] }
      if inv.blank?
        @product.errors.add("invoice product - #{invoicep[:id]}", _t("errors.invoice_product_not_found"), code: 497)
      else
        if invoice[:count].to_f % 1 > 0 && !@product.variant.product.is_fractionary
          @product.errors.add("invoice fractionary count - #{invoice[:id]}", _t("errors.invoice_count_cant_be_fractionary"), code: 499)
        elsif inv.balance < invoice[:count].to_f * @units_per_pack.to_f
          @product.errors.add("invoice product balance - #{invoice[:id]}", _t("errors.invoice_product_balance_less_than_required"), code: 498)
        else
          inv.lock!
          inv.balance                     -= invoice[:count].to_f * @units_per_pack.to_f
          inv.locked_balance_to_write_off += invoice[:count].to_f * @units_per_pack.to_f
        end
      end
    end

    if @product.errors.empty? && invoices.each(&:save!)
      locked_time = Time.now.to_s
      @current_shop.order_variants.only_offline_orders.need_to_write_off.not_locked
          .by_order_stock(@stock.id)
          .by_date(@from_date, @to_date)
          .by_unit(@unit_id)
          .by_variant(@variant_id)
          .by_price(@price)
          .by_units_per_pack(@units_per_pack)
          .update_all(locked_to_write_off: true, locked_to_write_off_at: locked_time)

      job = OfflineOrdersManualWriteOffsJob.perform_later(@current_shop.id, current_user.id, @stock.id, @from_date, @to_date, locked_time.as_json, @product.as_json, manual_write_off_params[:invoices].as_json)
      @current_shop.scheduled_jobs.offline_sales.create(job_id: job.provider_job_id)
      render_202
    else
      render status: 422, json: { errors: ErrorSerializer.serialize(@product.errors) }
    end
  end

  private

  def check_lock_status
    return render_422(501, _t("errors.you_need_to_wait_before_another_write_off_process_in_progress")) if @lock_status
  end

  def set_lock_status
    @lock_status = @current_shop.order_variants.only_offline_orders
          .by_order_stock(@stock.id).locked.maximum(:to_write_off_count).present?
  end

  def set_params_from_id
    begin
      @variant_id, @unit_id, @units_per_pack, @price, @from_date, @to_date, @stock_id = decode_id(params[:id])
    rescue
      render_422(495, _t("errors.offline_sales_cant_parse_id"))
    end
  end

  def set_product
    @product = find_product(@stock.id, @from_date, @to_date, @unit_id, @variant_id, @price, @units_per_pack)
    return render_404(495, _t("errors.offline_sales_product_not_found")) if @product.blank?
  end

  def set_stock
    @stock = find_stock(params[:stock_id] || @stock_id || auto_write_off_params[:stock_id])
    return render_404(405, _t("errors.stock_not_found")) if @stock.blank?
  end

  def find_product(stock_id, from_date, to_date, unit_id, variant_id, price, units_per_pack)
    @current_shop.order_variants.only_offline_orders.need_to_write_off.not_locked
        .by_order_stock(stock_id)
        .by_date(from_date, to_date)
        .by_unit(unit_id)
        .by_variant(variant_id)
        .by_price(price)
        .by_units_per_pack(units_per_pack)
        .group_and_order_for_offline_sales
        .first
  end

  def find_stock(stock_id)
    @current_shop.stocks.active# .by_user(current_user.id)
      .find_by(id: stock_id)
  end

  def set_data_structures(id)
    begin
      variant_id, unit_id, units_per_pack, price, from_date, to_date, stock_id = decode_id(id)
      return OpenStruct.new(stock_id: stock_id, from_date: from_date, to_date: to_date),
        OpenStruct.new(variant_id: variant_id, unit_id: unit_id, price: price, units_per_pack: units_per_pack)
    rescue
      @errors << { title: _t("errors.offline_sales_cant_parse_id"), code: 495 }
    end
  end

  def decode_id(id)
    YAML.load(Base64.decode64(id))
  end

  def manual_write_off_params
    params.require(:offline_sales).permit(invoices: [:id, :count])
  end

  def auto_write_off_params
    params.require(:offline_sales).permit(:stock_id, :from_date, :to_date)
  end
end
