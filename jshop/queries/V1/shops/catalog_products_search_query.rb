# frozen_string_literal: true

class V1::Shops::CatalogProductsSearchQuery < BaseQuery
  attr_accessor :params, :additional, :default_shop, :product, :products

  def initialize(_params = {}, _additional = {})
    @params       = _params
    @additional   = _additional
    @product      = set_product
    @default_shop = set_default_shop
  end


  def result
    @products = @default_shop.products.active.passed
                  .includes(:category, :tags, :unit, :active_product_barcodes)
    @products = search_by_params
    @products = order_and_paginate

    @products
  end

  private

  def page
    @page ||= params[:page] || 1
  end

  def per_page
    @per_page ||= params[:per_page] || 50
  end

  def sort
    @sort ||= "products.created_at desc"
  end

  def text_query
    @text_query ||= (params.dig(:name) || @product&.name)
  end

  def barcodes
    @barcodes ||= (params.dig(:barcodes) || @product&.active_product_barcodes)&.pluck(:barcode)
  end

  def product_id
    @product_id ||= params.dig(:product_id)
    @product_id == "all" ? nil : @product_id
  end

  def published
    @published ||= additional.dig(:published)
  end

  def products_shop_id
    @products_shop_id ||= additional.dig(:products_shop_id)
  end

  def shop_type
    @shop_type ||= additional.dig(:shop_type)
  end

  #####################
  #####################

  def search_by_params
    conditions = { is_active: true, product_mode: "passed", shop_id: @default_shop.id }

    if !published.blank?
      conditions.merge!(product_published: published)
    end

    if !product_id.blank?
      conditions.merge!(_not: { product_id: product_id })
    end

    ns = Variant.search(text_query, load: false, execute: false, misspellings: false,
          fields: [{ 'product_name^5': :word_middle }], where: conditions)

    bs = Variant.search(barcodes, load: false, execute: false,
          fields: [{ barcode_code: :phrase }], where: conditions)

    nj, bj = Searchkick.multi_search([ns, bs])

    product_ids = (nj.map(&:product_id) + bj.map(&:product_id)).uniq


    if !products_shop_id.blank?
      conditions = { shop_id: products_shop_id, is_verified: true, is_import: true, import_id: product_ids }
      siv_ids = Variant.search(load: false, where: conditions).pluck(:import_id).uniq

      product_ids -= siv_ids
    end

    @products.where(Product.arel_table[:id].in(product_ids))
  end


  def order_and_paginate
    @products.order(sort).page(page).per(per_page)
  end

  def set_product
    Product.active.find_by(id: product_id) if !product_id.blank?
  end

  def set_default_shop
    Shop.active.default.find_by(shop_type: shop_type)
  end
end
