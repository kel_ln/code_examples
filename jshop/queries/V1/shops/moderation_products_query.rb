# frozen_string_literal: true

class V1::Shops::ModerationProductsQuery < BaseQuery
  attr_accessor :params, :shop_type, :p_mode, :products

  def initialize(_params = {}, _additional = {})
    @params    = _params
    @p_mode    = _params[:product_mode]
    @shop_type = _additional[:shop_type]
  end

  def result
    @products = Product.active.preload(:shop, :category, :tags, :unit,
      :active_product_vendor_codes, :active_product_barcodes, :created_user)
    @products = by_shop_type
    @products = by_product_mode
    @products = by_created_shop
    @products = by_text_search
    @products = by_product_barcode

    case @p_mode
    when "pending"
      @products = by_created_date
    when "passed"
      @products = by_default_shop
      @products = by_tags
      @products = by_categories
      @products = by_published_status
    when "rejected"
    end

    @products = order_and_paginate

    @products
  end

  def totals
    @products = Product.active
    @products = by_shop_type

    @p_mode   = "pending"
    pending   = by_product_mode.size
    @p_mode   = "rejected"
    rejected  = by_product_mode.size
    @p_mode   = "passed"
    @products = by_product_mode
    passed    = by_default_shop.size

    return pending, passed, rejected
  end

  private

  def page
    @page ||= params[:page] || 1
  end

  def per_page
    @per_page ||= params[:per_page] || 50
  end

  # shops.name, products.name, products.created_at, products.created_user_id
  def sort
    @sort ||= case params[:sort]
              when "shop_name"
                "shops.name"
              when "name"
                "products.name"
              when "created_user"
                "products.created_user_id"
              else
                "products.created_at"
    end + (params[:asc].to_i == 1 ? " asc" : " desc")
  end


  def text_query
    @text_query ||= params.dig(:q)
  end

  def created_shop_id
    @created_shop_id ||= params.dig(:created_shop_id)
  end

  def default_shop_id
    Shop.active.default.find_by(Shop.arel_table[:shop_type].eq(@shop_type)).id
  end

  def barcode
    @barcode ||= params.dig(:barcode)
  end

  def barcode_type
    @barcode_type ||= params.dig(:barcode_type)
  end

  def created_date_from
    @created_date_from ||= params.dig(:created_date_from)
  end

  def created_date_to
    @created_date_to ||= params.dig(:created_date_to)
  end

  def tag_id
    @tag_ids ||= params.dig(:tag_id)&.split(",")
  end

  def category_id
    @category_ids ||= params.dig(:category_id)&.split(",")
  end

  # all, enabled, disabled
  def published_status
    @published_status ||= params.dig(:published_status)
  end

  #######################
  #######################

  def by_published_status
    case published_status
    when "disabled"
      @products.where(Product.arel_table[:is_published].eq(false))
    when "enabled"
      @products.where(Product.arel_table[:is_published].eq(true))
    else
      @products
    end
  end

  def by_categories
    return @products if category_id.blank?

    @products.where(Product.arel_table[:category_id].in(category_id))
  end

  def by_tags
    return @products if tag_id.blank?

    @products.joins(:tags).where(Tag.arel_table[:id].in(tag_id))
  end

  def by_created_date
    if created_date_from && created_date_to
      @products.where(Product.arel_table[:created_at]
          .between(Date.parse(created_date_from).beginning_of_day..Date.parse(created_date_to).end_of_day))
    elsif created_date_from
      @products.where(Product.arel_table[:created_at]
          .gteq(Date.parse(created_date_from).beginning_of_day))
    elsif created_date_to
      @products.where(Product.arel_table[:created_at]
          .lteq(Date.parse(created_date_to).end_of_day))
    else
      @products
    end
  end

  def by_product_barcode
    return @products if barcode_type.blank? || barcode.blank?

    @products.joins(:product_barcodes)
      .where(ProductBarcode.arel_table[:is_active].eq(true)
        .and(ProductBarcode.arel_table[:barcode_type].eq(barcode_type))
        .and(ProductBarcode.arel_table[:barcode].eq(barcode))
       )
  end

  def by_default_shop
    @products.where(Product.arel_table[:shop_id].eq(default_shop_id))
  end

  def by_created_shop
    return @products if created_shop_id.blank?

    if @p_mode == "passed"
      @products.where(Product.arel_table[:created_shop_id].eq(created_shop_id))
    else
      @products.where(Product.arel_table[:shop_id].eq(created_shop_id))
    end
  end

  def by_text_search
    return @products if text_query.blank?

    conditions = { is_active: true, product_mode: @p_mode }

    case published_status
    when "disabled"
      conditions.merge!(product_active: false)
    when "enabled"
      conditions.merge!(product_active: true)
    else
    end

    if @p_mode == "passed"
      conditions.merge!(shop_id: default_shop_id)
    elsif !created_shop_id.blank?
      conditions.merge!(shop_id: created_shop_id)
    end

    ids = Variant.search(text_query, load: false, fields: [{ 'product_name^5': :word_middle }],
                          where: conditions).pluck(:product_id).uniq
    @products.where(Product.arel_table[:id].in(ids))
  end

  def by_shop_type
    @products.joins(:shop)
    .where(Shop.arel_table[:shop_type].eq(@shop_type)
      .and(Shop.arel_table[:is_active].eq(true))
    )
  end

  def by_product_mode
    @products.where(Product.arel_table[:product_mode].eq(@p_mode))
  end

  def order_and_paginate
    @products.order(sort).page(page).per(per_page)
  end
end
