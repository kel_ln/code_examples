# frozen_string_literal: true

class SellPricesCalculateJob < ApplicationJob
  queue_as :default

  # цена товара зависит от
  # invoice_product_id
  # price_id
  # variant_id -> product
  # shop_currency_rate_id

  # цена действуюет с даты
  # цены формируются по накладным; если накладной нет то и цены не будет
  # !!! цены нужно формировать только для накладных с положительным балансом !!!
  # если в накладную вернули баланс должен пройти пересчет цен под новый

  # ??? ЦЕНЫ ВЫСЧИТЫВАЕМ ТОЛЬКО ДЛЯ ВЕБ ПРАЙСОВ И ТОВАРОВ С ФИКС ЦЕНОЙ ???

  # проход по накладным

  # 3 вида изменений создание/обновление/удаление

  # тк прайсы действуют на все вложенные категории
  # категории товара учитывают вложенность

  # воркер работает только с накладными, все остальные свзанные таблица собирают данные
  # и создают воркеры по накладным

  # универсальная схема действует исходя из текущих данных не зная о том какие изменения были внесены
  # воркер получает данные что в записи были обновления
  # находим сам обьект и проверяем не удалился ли он это проверка на is_active
  # если да то то находим связанные с этим обьектом записи и удаляем
  # если обьект активный значит были изменения в самом обьекте
  # от основного обьекта нужно постороить матрицу цен и сравнить с имещимися записями

  # огграничить расчет цен только для магазинов spree

  def perform(shop_id, object_id, object_type)
    shop = Shop.find_by(id: shop_id)

    return if shop.spree_shops.blank?

    object = class_eval(object_type).find_by(id: object_id, shop_id: shop_id)

    if ShopLock.is_locked?(object.id, shop.id)
      SellPricesCalculateJob.set(wait: 5.minute).perform_later(shop_id, object_id, object_type)
    else
      begin
        ShopLock.lock!(object_id, shop_id, "Update variant prices")

        case object_type
        when "InvoiceProduct"
          current_sales_prices = shop.variant_sell_prices.active.where(invoice_product_id: object.id)

          # если текущая накаладная черновик, запись игнорируется
          return if object.document.drafted?

          # в случае если был изменен склад все текущие варианты подлежат удалению
          if !current_sales_prices.blank? && object.document.stock_id != current_sales_prices[0].stock_id
            current_sales_prices.each { |t| t.is_active = false; t.save; }
          end

          # записи о ценах не может быть есть нет активных накладаных или баланс партии исчерпан
          if !object.is_active || object.document.deleted? || object.balance <= 0
            current_sales_prices.each { |t|
              t.balance   = object.balance
              t.is_active = false
              t.save
            }
          else
            variant = object.product
            sell_prices = []
            # если есть фиксированная цена прайсы игнорируются
            # матрица цен состоит только из одной записи
            if !variant.retail_price.blank?
              sell_prices << {
                variant_id:         variant.id,
                product_id:         variant.product_id,
                is_default_variant: variant.is_default,
                stock_id:           object.document.stock_id,
                shop_id:            shop.id,
                invoice_product_id: object.id,
                currency_id:        object.document.currency_id,
                cost_price:         object.per_unit_cost_price,
                sell_price:         variant.retail_price,
                balance:            object.balance,
                apply_at:           object.document.document_date,
                document_date:      object.document.document_date
              }
            else
              # курсы валют
              # курсы валют действующие выделяются последний действующий курс
              # и все курсы созданнные на будущие даты
              # кроме основной валюты магазина

              shop_currency       = shop.shop_currencies.find_by(currency_id: object.document.currency_id, is_default: false)
              shop_currency_rates = shop_currency&.rates

              rates = if !shop_currency_rates.blank?
                # курс вылют действующий на текущий момент
                s  = shop_currency_rates.select { |t| t.apply_at <= Time.current }
                # ! если курсы валют не заданы значит используется отношение 1:1
                s  = s.blank? ? [{ value: 1.0, apply_at: nil }] : s.first

                # курсы валют на будущие даты
                sf = shop_currency_rates.select { |t| t.apply_at > Time.current }

                sfg, sdate = [], nil
                sf.each do |t|
                  next if sdate == t.apply_at
                  sdate = t.apply_at
                  sfg << t
                end

                ([s] + sfg).compact
              else
                [{ value: 1.0, apply_at: nil }]
              end

              category           = variant.product.category
              category_path_ids  = category.path_ids

              # !!! только прайсы веб категорий !!!

              prices = shop.prices.with_category_ancestry_depth.active
                    .where(category_id: category_path_ids, is_web: true)
                    .order(category_ancestry_depth: :desc, start_date: :desc, created_at: :desc)

              price_lists = if !prices.blank?
                # прайс лист действующий сейчас
                pl  = prices.select { |t| t.start_date <= Time.current }&.first

                # прайс листы на будущие даты
                plf = prices.select { |t| t.start_date >  Time.current }

                # если сейчас действует прайс лист
                # на товар может действовать прайс только такогоже либо более высокого уровня
                plf = plf.select { |t| t["category_ancestry_depth"] >= pl["category_ancestry_depth"] } if !pl.blank?

                plfg, min_lvl, sdate = [], 0, nil
                # из оставшегося списка прайслистов выстроить кривую по датам
                if !plf.blank?
                  prices = shop.prices.with_category_ancestry_depth.active
                      .where(id: plf.map(&:id))
                      .order(start_date: :asc, created_at: :desc)

                  prices.each do |t|
                    next if min_lvl > t["category_ancestry_depth"]
                    next if sdate == t.start_date

                    sdate   = t.start_date
                    min_lvl = t["category_ancestry_depth"]
                    plfg << t
                  end
                end

                ([pl] + plfg).compact
              end

              # если прайсов нет то цен тоже не будет
              if !price_lists.blank?

                calculator = Dentaku::Calculator.new

                # даты это точки кривой изменения цен
                dates = (rates.pluck(:apply_at) + price_lists.pluck(:start_date)).compact.uniq

                r_ind, pl_ind = 0, 0

                dates.each do |date|

                  r_ind  += 1 if !rates[r_ind + 1].blank?        && rates[r_ind + 1].apply_at          == date
                  pl_ind += 1 if !price_lists[pl_ind + 1].blank? && price_lists[pl_ind + 1].start_date == date

                  r  = rates[r_ind]
                  pr = price_lists[pl_ind]

                  sp = calculator.evaluate(pr.converted_formula, price: (object.per_unit_cost_price * r[:value]))
                  sp = sp.to_d.round_by_mask(pr.price_rounding.mask)

                  sell_prices << {
                    variant_id:            variant.id,
                    product_id:            variant.product_id,
                    is_default_variant:    variant.is_default,
                    stock_id:              object.document.stock_id,
                    shop_id:               shop.id,
                    invoice_product_id:    object.id,
                    currency_id:           object.document.currency_id,
                    shop_currency_rate_id: r[:id],
                    price_id:              pr.id,
                    is_public_price_list:  pr.is_public,
                    is_web_price_list:     pr.is_web,
                    cost_price:            object.per_unit_cost_price,
                    sell_price:            sp,
                    balance:               object.balance,
                    apply_at:              date,
                    document_date:         object.document.document_date
                  }

                end
              end
            end

            # сравнивает матрицу вариантов с текущими записями
            # удаляем не актуальные записи
            active_ids = []

            sell_prices.each do |sp|

              csp = current_sales_prices.detect { |t|
                t.invoice_product_id == sp[:invoice_product_id] &&
                t.variant_id == sp[:variant_id] &&
                t.price_id == sp[:price_id] &&
                t.shop_currency_rate_id == sp[:shop_currency_rate_id] &&
                t.is_active == true
              }

              if csp.blank?
                shop.variant_sell_prices.create(sp)
              else
                active_ids << csp.id
                csp.update_attributes(sp)
              end
            end

            current_sales_prices.select { |t| !active_ids.include?(t.id) }.each do |csp|
              csp.is_active = false
              csp.save
            end

          end
        when "Document"
          object.invoice_products.each do |ip|
            SellPricesCalculateJob.perform_later(shop.id, ip.id, "InvoiceProduct")
          end
        when "ShopCurrencyRate"
          invoice_products = InvoiceProduct.joins(:document).where(
            Document.arel_table[:status].eq("saved")
            .and(Document.arel_table[:shop_id].eq(shop_id))
            .and(Document.arel_table[:currency_id].eq(object.shop_currency.currency_id))
            .and(InvoiceProduct.arel_table[:is_active].eq(true))
            .and(InvoiceProduct.arel_table[:balance].gt(0))
          )

          invoice_products.each do |ip|
            SellPricesCalculateJob.perform_later(shop.id, ip.id, "InvoiceProduct")
          end
        when "Price"
          # только веб прайсы
          return if !object.is_web

          price_category_tree_ids = object.category.subtree_ids

          invoice_products = InvoiceProduct.joins(:document, product: :product).where(
            Document.arel_table[:status].eq("saved")
            .and(Document.arel_table[:shop_id].eq(shop_id))
            .and(InvoiceProduct.arel_table[:is_active].eq(true))
            .and(InvoiceProduct.arel_table[:balance].gt(0))
            .and(Product.arel_table[:category_id].in(price_category_tree_ids))
          )

          invoice_products.each do |ip|
            SellPricesCalculateJob.perform_later(shop.id, ip.id, "InvoiceProduct")
          end
        when "Variant"
          invoice_products = InvoiceProduct.joins(:document).where(
            Document.arel_table[:status].eq("saved")
            .and(Document.arel_table[:shop_id].eq(shop_id))
            .and(InvoiceProduct.arel_table[:is_active].eq(true))
            .and(InvoiceProduct.arel_table[:balance].gt(0))
            .and(InvoiceProduct.arel_table[:product_id].eq(object.id))
          )

          invoice_products.each do |ip|
            SellPricesCalculateJob.perform_later(shop.id, ip.id, "InvoiceProduct")
          end
        when "Product"
          variants_ids = object.variants.all_active.pluck(:id)

          invoice_products = InvoiceProduct.joins(:document).where(
            Document.arel_table[:status].eq("saved")
            .and(Document.arel_table[:shop_id].eq(shop_id))
            .and(InvoiceProduct.arel_table[:is_active].eq(true))
            .and(InvoiceProduct.arel_table[:balance].gt(0))
            .and(InvoiceProduct.arel_table[:product_id].in(variants_ids))
          )

          invoice_products.each do |ip|
            SellPricesCalculateJob.perform_later(shop.id, ip.id, "InvoiceProduct")
          end
        when "Category"
          invoice_products = InvoiceProduct.joins(:document, product: :product).where(
            Document.arel_table[:status].eq("saved")
            .and(Document.arel_table[:shop_id].eq(shop_id))
            .and(InvoiceProduct.arel_table[:is_active].eq(true))
            .and(InvoiceProduct.arel_table[:balance].gt(0))
            .and(Product.arel_table[:category_id].eq(object.id))
          )

          invoice_products.each do |ip|
            SellPricesCalculateJob.perform_later(shop.id, ip.id, "InvoiceProduct")
          end
        end
      rescue
        SellPricesCalculateJob.set(wait: 5.minute).perform_later(shop_id, object_id, object_type)
        nil
      ensure
        ShopLock.unlock!(object.id, shop.id)
      end
    end
  end
end
