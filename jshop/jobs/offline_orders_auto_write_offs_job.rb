# frozen_string_literal: true

class OfflineOrdersAutoWriteOffsJob < ApplicationJob
  queue_as :default

  # автоматическое списание
  # products это массив сгруппированных продуктов для поиска
  # для каждой сгруппированной партии продуктов
  # подбираются накладные в отсортированные по типу списания
  # списывается весь возможный баланс
  # если баланса не достаточно создается доп позиция в чеке
  # идет пересчет общей суммы счета
  # создается документ списание

  def perform(shop_id, user_id, stock_id, from_date, to_date, locked_time)
    total 1000
    at 0, "job started"

    shop = Shop.find_by(id: shop_id)
    Time.zone = shop.city.timezone

    stock = shop.stocks.find_by(id: stock_id)

    write_off = shop.write_offs.build(stock_id: stock.id, date: Time.now,
            responsible_user_id: user_id, is_offline_sales: true, is_readonly: true)

    shop_rates = shop.get_currency_rate_ratio(write_off.date)

    at 10, "calculate products to write off"

    grouped_products = shop.order_variants.only_offline_orders.need_to_write_off.locked
                            .by_locked_at(locked_time)
                            .by_order_stock(stock.id)
                            .by_date(from_date, to_date)
                            .group_and_order_for_offline_sales

    sprc  = 100
    delta = 800 / (grouped_products.to_a.size + 1)

    ########
    grouped_products.each do |product|

      at (sprc + delta), "writing off -> #{product.variant.get_variant_name}"

      order_variants = shop.order_variants.only_offline_orders.need_to_write_off.locked
              .by_locked_at(locked_time)
              .by_order_stock(stock.id)
              .by_date(from_date, to_date)
              .by_unit(product.unit_id)
              .by_variant(product.variant_id)
              .by_price(product.price)
              .by_units_per_pack(product.units_per_pack)
              .order("orders.date asc")
              .includes(:order)

      invoice_products = shop.invoice_products.active.active_document.positive_balance
                      .by_stock(stock.id).where(product_id: product.variant_id)
                      .sort_by_stock_write_off_type(stock.write_off_type).preload(:document)

      ind = 0
      order_variants.each do |ordvar|

        c     = ordvar.to_write_off_count
        order = ordvar.order

        loop do
          order_product = ordvar
          wofc = 0
          inv  = invoice_products[ind]

          break if inv.blank? # закончились накладные выход из цикла

          order_product.cost_price      = inv.per_unit_cost_price * shop_rates[inv.document.currency_id][:ratio]
          order_product.batch_object_id = inv.id

          if (c * order_product.units_per_pack) <= inv.balance # если количество нужное к списанию меньше или равно остатку на балансе
            wofc = c
            order_product.to_write_off_count = c = 0
          else # списываем столько сколько есть в партии, для остального создаем доп позицию в счете
            order_product.count = inv.balance
            order_product.to_write_off_count = 0
            order_product.convert_to_units

            # с позиции не может быть возвращено больше чем продано остаток сбросить на новую созданную позицию
            to_ref = 0
            if order_product.refunded_count > order_product.count
              to_ref = order_product.refunded_count - order_product.count
              order_product.refunded_count = order_product.count
              order_product.refunded_count_in_units = order_product.refunded_count * order_product.units_per_pack
            end

            c   -= order_product.count
            wofc = order_product.count

            # создание дополнительной позиции в счете ( для еще не списанных товаров )
            ordvar = order.order_variants.build order_product.attributes.except("id", "count", "created_at",
                "updated_at", "batch_object_id", "locked_to_write_off", "locked_to_write_off_at")

            ordvar.count                  = c
            ordvar.to_write_off_count     = c
            ordvar.cost_price             = ordvar.offline_cost_price
            ordvar.sales_commission_total = 0

            # перераспределние возвратов
            ordvar.refunded_count          = to_ref
            ordvar.refunded_count_in_units = to_ref * ordvar.units_per_pack

            ordvar.convert_to_units
            ordvar.calculate_totals

            ordvar.save
          end

          # пересчитываем счет
          order_product.calculate_totals
          # пересчитываем комиссию продавцу
          order_product.calculate_sales_commission_total
          # если были возвраты снимаем начисления коммисионных
          if order_product.refunded_count_in_units > 0
            order_product.sales_commission_total -= ((order_product.sales_commission_total / order_product.count_in_units) * order_product.refunded_count_in_units)
          end

          # снимаем блокировку
          order_product.locked_to_write_off = false
          order_product.locked_to_write_off_at = nil

          order_product.save

          # создается запись о списании товара
          write_off_product = write_off.write_off_products.build(
            shop_id: shop.id,
            shop_tax_id: order_product.shop_tax_id,
            variant_id: order_product.variant_id,
            object_product_id: inv.id,
            count: wofc,
            unit_id: order_product.unit_id, unit_type: order_product.unit_type,
            document_currency_id: inv.document.currency_id,
            shop_currency_rate_id: shop_rates[inv.document.currency_id][:id],
            document_price: inv.per_unit_cost_price,
            purchase_price:  order_product.cost_price,
            expiration_date: inv.expiration_date,
            order_variant_id: order_product.id
          )
          write_off_product.convert_to_units
          write_off_product.calculate_amount

          inv.lock!
          inv.balance -= (wofc * order_product.units_per_pack)                       # уменьшаем количество на балансе партии
          inv.locked_balance_to_write_off += (wofc * order_product.units_per_pack)   # блокируем баланс для списания
          inv.save!

          ind += 1 if inv.balance < order_product.units_per_pack # если в партии меньше чем минимальное количество в упаковке переключаемся на след партию
          break if c == 0 # выход из цикла, списано все количество необходимое для счета
        end

        # пересчет сумм для счета себестоймости и комиссии продавцу
        order.variants_cost_total      = order.order_variants.sum(&:cost_price_total)
        order.sales_commission_total   = order.order_variants.sum(&:sales_commission_total)
        order.save
      end

      # снимаем блокировку со счетов ( для отображаения в общей таблицы  )
      order_variants.update_all(locked_to_write_off: false, locked_to_write_off_at: nil)

    end
    ############

    at 900, "saving write off"

    write_off.total_cost = write_off.write_off_products.sum(&:amount)
    write_off.write_off_realization(true) if write_off.valid? && write_off.save

    at 1000, "job ending"

    # Sidekiq::Status.delete(self.provider_job_id)
    ScheduledJob.find_by(job_id: self.provider_job_id, shop_id: shop_id).delete
  end
end
