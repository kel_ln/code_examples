# frozen_string_literal: true

class InvoiceProductImportFromGeneralCatalogJob < ApplicationJob
  queue_as :default

  def perform(invoice_product_id, shop_id)
    I18n.locale = :ru

    @current_shop = Shop.find_by(id: shop_id)
    @default_shop = Shop.find_by(is_default: true, shop_type: @current_shop.shop_type)

    invoice_product = @current_shop.invoice_products.find_by(id: invoice_product_id)

    return if !invoice_product.is_import

    default_variant = @default_shop.variants.find_by(id: invoice_product.import_product_id)
    default_product = default_variant.product

    if ShopLock.is_locked?(default_product.id, @current_shop.id)
      InvoiceProductImportFromGeneralCatalogJob.set(wait: 1.minute).perform_later(invoice_product_id, shop_id)
    else

      begin
        ShopLock.lock!(default_product.id, @current_shop.id, "import product")

        product = V1::ImportGeneralProductService.call(@current_shop, @default_shop, default_product)

        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # !!!! замена продукта в накладной !!!!
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        variant_id = if default_variant.is_default?
          product.default_variant.id
        else
          variant_product_option_type_values_ids = []

          default_variant.variant_option_type_values.each do |variant_option_type_value|
            product_option_type_value = variant_option_type_value.product_option_type_value
            option_type               = product_option_type_value.product_option_type.option_type

            opt  = @current_shop.option_types.active.where(name: option_type.name).first_or_create!
            pot  = product.product_option_types.active.where(option_type_id: opt.id).first_or_create!
            potv = pot.product_option_type_values.active.where(name: product_option_type_value.name).first_or_create!

            variant_product_option_type_values_ids << potv.id
          end

          pot_map = {}
          product.variants.active.map { |b| pot_map[b.id] = b.variant_option_type_values.pluck(:product_option_type_value_id) }
          cv_id = pot_map.detect { |k, v| (v - variant_product_option_type_values_ids).blank? }&.first

          if cv_id.blank?
            vr = product.variants.create!(vendor_code: variant.vendor_code, is_published: variant.is_published,
                                          short_name: variant.short_name, shop_id: @current_shop.id)
            variant_product_option_type_values_ids.each do |vpoi|
              vr.variant_option_type_values.create!(product_option_type_value_id: vpoi, shop_id: @current_shop.id)
            end
            cv_id = vr.id
          end
          cv_id
        end

        unit = if invoice_product.unit_type == "Unit"
          invoice_product.unit
        else
          dpp = default_product.product_packs.find_by(invoice_product.unit_id)
          product.product_packs.find_by(count: dpp.count, count_in_units: dpp.count_in_units, name: dpp.name)
        end

        #############
        # обновление позиции в накладной
        # после обновления позиция проиндексируется и появится в поиске
        #############

        invoice_product.is_import         = false
        invoice_product.import_product_id = nil
        invoice_product.product_id        = variant_id
        invoice_product.unit_id           = unit.id
        invoice_product.save!

      rescue
        InvoiceProductImportFromGeneralCatalogJob.set(wait: 1.minute).perform_later(invoice_product_id, shop_id)
        nil
      ensure
        ShopLock.unlock!(default_product.id, @current_shop.id)
      end

    end
  end
end
