# frozen_string_literal: true

class LoyaltyCardsGenerateJob < ApplicationJob
  queue_as :default

  def perform(loyalty_card_serie_id, shop_id)
    lcs  = LoyaltyCardSerie.find_by(id: loyalty_card_serie_id, shop_id: shop_id)
    shop = Shop.find_by(id: shop_id)

    if ShopLock.is_locked?(lcs.id, lcs.shop_id)
      ScheduledJob.find_by(job_id: self.provider_job_id, shop_id: shop_id).delete

      job = LoyaltyCardsGenerateJob.set(wait: 5.minute).perform_later(lcs.id, lcs.shop_id)
      lcs.shop.scheduled_jobs.loyalty_cards.create(job_id: job.provider_job_id, object_id: lcs.id)
    else
      begin
        ShopLock.lock!(lcs.id, lcs.shop_id, "generate loyalty cards")

        # если карты начали создавать в другом потоке выход
        return if shop.loyalty_cards.find_by(loyalty_card_serie_id: lcs.id)

        (lcs.first_number.to_i..lcs.last_number.to_i).each_slice(10000) do |v|
          ActiveRecord::Base.connection.execute("INSERT INTO loyalty_cards (card_number, shop_id, loyalty_card_serie_id, created_at, updated_at)
                  SELECT x.number, '#{shop_id}', '#{loyalty_card_serie_id}', now(), now()
                  FROM generate_series(#{v[0]}, #{v[-1]}) AS x(number);")
        end
        lcs.loyalty_cards.reindex

      rescue
        job = LoyaltyCardsGenerateJob.set(wait: 5.minute).perform_later(lcs.id, lcs.shop_id)
        lcs.shop.scheduled_jobs.loyalty_cards.create(job_id: job.provider_job_id, object_id: lcs.id)
        nil
      ensure
        ScheduledJob.find_by(job_id: self.provider_job_id, shop_id: shop_id).delete
        ShopLock.unlock!(lcs.id, lcs.shop_id)
      end
    end
  end
end
